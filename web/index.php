<?php

@session_start();
error_reporting(0);
defined('YII_DEBUG') or define('YII_DEBUG', false); // true / false
defined('YII_ENV') or define('YII_ENV', 'prod'); // dev / prod

DEFINE("ROOT",__DIR__.'/../');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();
