<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

use app\models\RetailCRM;
use app\models\Storeland;
use app\models\Accounts;
use app\models\OrdersStatuses;
use app\models\DeliveryTypes;
use app\models\Payments;
use app\models\OrderSync;
use app\models\Settings;
use app\models\Orders;

class OrdersController extends Controller
{
	public function actionTest()
	{
		//echo '123';
	}
	
    public function actionSynchronize(){

        $accs_to_sync = new \app\models\OrderSync;
        $accs_to_sync = $accs_to_sync->findAll(['warning' =>['='=>'0'],'enabled'=>['='=>'1']]);   
        
        foreach ($accs_to_sync as $acc){    
           
            $site    = $acc['shop'];
            $catalog = $acc['catalog'];
            $prefix  = $acc['prefix'];
            $hash    = $acc->attributes['uniqueAccountHash'];
            $date    = $acc->attributes['lastDate'];
            $newdate = new \DateTime();
            $now     = new \DateTime();
            $now     = $now->format('Y-m-d H:i:s');
            $acc->lastDate = $now;
            $acc->save();
            $account= \app\models\Accounts::getModel($hash);
            if(!($account['active'] == 1 && $account['freeze'] == 0)){
                echo 'account not active';
                continue;
            }
			$checkConnection =  \app\models\Accounts::checkConnection($account);
            if($checkConnection==true){
                if($account == null){                 
                    continue;
                }
                else
                {
                    $delivery = new \app\models\DeliveryTypes;
                    $statuses = new \app\models\OrdersStatuses;
                    $payment =  new \app\models\Payments();
  
                    $payment= $payment->findAll(['uniqueAccountHash'=>$hash]);
                    $statuses = $statuses->findAll(['uniqueAccountHash'=>$hash]);
                    $delivery = $delivery->findAll(['uniqueAccountHash'=>$hash]);

                    if($payment == null || $statuses == null || $delivery == null){
                        print 'Настройки не обнаружены'."\r\n";
                        continue;
                    }

                    $apiUrl = $account['retailAPIURL'];
                    $apiKey = $account['retailAPIKey'];                    
                    
                    $url = $account['storelandAPIURL'];
                    $secret_key = $account['storelandAPIToken'];
                        
                    $storeland = new \app\models\Storeland($url, $secret_key);

                    $retail = new \app\models\RetailCRM($apiUrl, $apiKey,$site);
                    $orders = $storeland->getOrders(10,$date);
                    if($orders == null){
                        print 'Заказов не найдено'."\r\n";
                        continue;
                    }else{
                        echo "Выгрузка заказов ".$url." (".count($orders).")\n";
                        foreach ($orders as $order){
                            try{
                                $ids = $retail->create($order,$hash,$site,$catalog,$prefix);
                                if ($ids == -1){
                                    continue;
                                }
                                if($ids !== FALSE){
                                    $appModelsOrders =  new \app\models\Orders;
                                    $appModelsOrders->uniqueAccountHash = $hash;
                                    $appModelsOrders->retailId = (string)$ids['retail'];
                                    $appModelsOrders->externalId = (string)$ids['storeland'];
                                    if($ids['retail'] == 'не удалось создать'){
                                        $appModelsOrders->errors = 1;
                                        $appModelsOrders->log = $ids['log'];
                                        $appModelsOrders->response = $ids['response'];
                                    }else{
                                        $appModelsOrders->errors = 0;
                                    }
                                    $appModelsOrders->shop = $acc->attributes['shop'];
                                    $appModelsOrders->direction = '0';

                                    $appModelsOrders->save();
                                    if(!$appModelsOrders->save())
                                        print_r($appModelsOrders->getErrors());
                                }else{
                                    continue;
                                }
                            }catch (\Exception $exception){

                            }
                        }
                    }
                }
            }                       
        }
    }           
}