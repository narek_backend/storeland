<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

use app\models\RetailCRM;
use app\models\Storeland;
use app\models\Accounts;
use app\models\OrdersStatuses;
use app\models\DeliveryTypes;
use app\models\Payments;
use app\models\OrderSync;
use app\models\Settings;
use app\models\Orders;

class DeleteController extends Controller
{
    public function actionOrders(){
        $accounts = new \app\models\Accounts;
        $accounts = $accounts->find()->all();
        
        $date = strtotime(date('Y-m-d H:i:s'))-(60*60*24*31);
        $date = date('Y-m-d H:i:s',$date);
        foreach ($accounts as $account){

            $orders = new \app\models\Orders();
            $orders = $orders->find()->where('uniqueAccountHash = "'.$account->attributes['uniqueAccountHash'].'" AND lastDate > "' .$date.'"')->all();
            foreach ($orders as $order){
              $result = $order->delete();
              var_dump($result);
            }           
        }
    }
}