<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ModuleController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }
    
    public function actionActivate(){
        $client = new \RetailCrm\ApiClient('https://u5904sbar-mn1-justhost.retailcrm.ru','65eLEPv6CZ0epef0BNutkytYoLeI4dNN' );
        
        $parameters = array(
            'clientId' => \Yii::$app->params['clientId'],
            'integrationCode' => \Yii::$app->params['integrationCode'],
            'code' => \Yii::$app->params['code'],            
            'active' => 'true',            
            'baseUrl' => \Yii::$app->params['baseUrl'],
            'accountUrl' => \Yii::$app->params['accountUrl'],
            'systemUrl' =>\Yii::$app->params['systemUrl'],
            'name' => 'Интеграция Storeland c RetailCRM',
            'actions' => [												// Относительные пути от базового URL до конкретных методов (массив "Код метода": "Путь", допустимые методы: activity)
            'activity' => '/accounts/activate',],
        
        );
        $result = $client->request->integrationModulesEdit($parameters);

        $this->stdout('result: ' . var_export($result, true));

    }
}



?>