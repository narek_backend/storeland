<?php

return [
    'adminEmail' => 'admin@example.com',
    'crmIntegrationCode' => 'storelandcrm',
    'neededCredentials' => [
        '/api/orders',  
        '/api/orders/create',
        '/api/orders/{externalId}',
        '/api/orders/{externalId}/edit',
        '/api/integration-modules/{code}',
        '/api/integration-modules/{code}/edit',
        '/api/reference/delivery-types',
        '/api/reference/payment-statuses',
        '/api/reference/payment-types',
        '/api/reference/statuses',
        '/api/reference/sites',
        '/api/customers'
    ],
    'integrationModule' => [  
        'code' => 'storelandcrm',
        'active' => 'true',
        'clientId' => 'd4ee0fbbeb7ffd4fd7a7d477a7ecd822',
        'baseUrl' => 'http://storeland.imb-service.ru',
        'accountUrl' => 'http://storeland.imb-service.ru/accounts',        
        //'systemUrl' => 'https://testirovanie.retailcrm.ru',
        'name' => 'Storeland',
    ],
        

];
