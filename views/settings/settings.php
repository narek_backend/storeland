<?php
use yii\helpers\Html;
use yii\widgets\ListView;
$this->title = Yii::t('app', 'Настройки соответствий');  
?> 
<div class="row">
    <div class="col-lg-12">
        <div class="blog-main">
            <div class="lead"> 
            
            <table class="table table-bordered" style="margin-top: 20px">
                <thead>
                    <tr>                          
                        <th><a href="http://storeland.imb-service.ru/settings/status" class = "btn btn-lg btn-light d-block">
                            Настройка соотвествия статусов
                        </a>
                        </th>
                    </tr>
                    <tr>                          
                        <th><a href="http://storeland.imb-service.ru/settings/delivery" class = "btn btn-lg btn-light d-block">
                            Настройка соотвествия типов доставки
                            </a>
                        </th>
                    </tr>
                    <tr>
                    <th><a href="http://storeland.imb-service.ru/settings/payments" class = "btn btn-lg btn-light d-block">
                         Настройка соотвествия типов оплаты </a> 
                        </th>
                    </tr>
                        <tr>
                        <th>
                            <a href="http://storeland.imb-service.ru/settings/payment-statuses" class = "btn btn-lg btn-light d-block">
                             Настройка соотвествия статусов оплаты </a> 
                        </th>
                    </tr>
                         
                           

                    
                </thead>
                <tbody>  
                </tbody>  
            </table>    
 
              
            </div>
        </div>
    </div>    
</div>  
 <?php 

 if (is_array($warning )){?>
    <div class="align-text-bottom">
        <p class="lead">
            Обратите внимание,что перейти к запуску синхронизации заказов нельзя, так как не все настройки соответсвий настроены:
        </p>
        <?php foreach ($warning as $key=>$value){?>
        <ul><?=$key?>
            <li><?= $value?></li>
        </ul>    
        <?php }?>
    </div>
    <?php }else{ ?>
    <div class="col-lg-1">
        <a onclick ="sync()" class ="btn btn-lg btn-info">
            Перейти к синхронизации
        </a>
    </div>
    <script>
        function sync(){
                window.location.reload(true);
                window.location.replace('http://storeland.imb-service.ru/integration');            
        }
    </script>
<?php } ?>

    <?php
    require_once ROOT.'views/layouts/active.php';