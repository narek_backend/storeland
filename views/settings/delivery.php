<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $retailDeliveryTypes array */
/* @var $storelandDeliveryTypes array */
/* @var $shops array */
$this->title = Yii::t('app', 'Соответствие типов доставки ');
?>
<div class="sites-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php 
    if(!$retailDeliveryTypes){?>
        <div class = 'alert-danger'>
            <h1>Не удалось получить типы доставок из retailCrm</h1>
            <p class = 'lead'>
                Возможно,стоит проверить Ваш АПИ-ключ
            </p>    
        </div>
     </div> 
    <?php    
       
        }else{
    ?>
    <?php 
    if(!$storelandDeliveryTypes){?>
        <div class = 'alert-danger'>
            <h1>Не удалось получить типы доставок из Storeland</h1>
            <p class = 'lead'>
                <a href="http://storeland.imb-service.ru/info">Пройдитесь по пунктам документации </a> и проверьте, всё ли правильно Вы делаете
            </p>    
        </div>    
</div>
    <?php           
        }else{ 
            ?>   
    
     <div class="row">
        <div class="col-lg-12">
            <?php 
            $save = (isset($_GET['save']))? (($_GET['save'] == 'true')? 'true' : ''):'';
                echo ($save == 'true')?
                     '<div class="alert alert-success">
                        <h1>Изменения успешно применены</h1>
                    </div>
            ' : ''  ;
                   echo ($save == 'false')?
                    '<div class="alert alert-danger">
                        <h1>Не удалось применить изменения</h1>
                    </div>
            ' : ''  ;
             
            ?>
            
            </div>
            <div class="sites-form">
                <?= Html::beginForm(['/settings/delivery'], 'post') ?>

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Типы доставок в Storeland</th>
                            <th>Типы доставок в retailCRM</th>
                        </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($storelandDeliveryTypes as $key => $value) { ?>
                        <tr>
                            <td>
                                <?= $value ?>
                                <?= Html::hiddenInput('storelandDeliveryTypes[]', $key ) ?>
                            </td>
                            <td>
                                <?php
                                $options = ['class' => 'form-control'];
                                if($model != ''){                                    
                                    foreach($model as $deliveryloc){
                                       
                                        if($deliveryloc->attributes['storelandDeliveryTypes'] == $key){
                                            $options = ['class' => 'form-control','options' =>[$deliveryloc->attributes['retailDeliveryTypes'] =>['selected' => true]]];
                                            break;
                                        }else{
                                            $options = ['class' => 'form-control'];
                                        }
                                    }
                                }
                                ?>
                                
                                <?=  ($model == '')?                                    
                                        Html::dropDownList('retailDeliveryTypes['.$key.']',[],$retailDeliveryTypes,$options):
                                        Html::dropDownList('retailDeliveryTypes['.$key.']',[],$retailDeliveryTypes,$options);                                     
                                ?> 
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn btn-success']) ?>
                </div>
                <div class="form-group">
                    <a onclick="confirm_delete()"class ="btn btn btn-danger">
                        Очистить настройки 
                    </a>
                </div>
                <script>
                    function confirm_delete(){
                        var isAdmin = confirm("Вы дейстительно хотите очистить соответсия?");
                        if(isAdmin == true){
                            window.location.reload(true);
                            window.location.replace('http://storeland.imb-service.ru/settings/delivery?delete=true');
                        }
                    }
                </script>
            </div>                
        </div>             
    </div>
</div>

            
        <?php }
    }
    ?>
    
  <?php
    require_once ROOT.'views/layouts/active.php';