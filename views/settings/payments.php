<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $retailPaymentTypes array */
/* @var $storelandPaymentTypes array */
/* @var $shops array */
$this->title = Yii::t('app', 'Соответствие типов оплаты ');
?>
<div class="sites-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php 
    if(!$retailPaymentTypes){?>
        <div class = 'alert-danger'>
            <h1>Не удалось получить типы оплат из retailCRM </h1>
            <p class = 'lead'>
                Возможно,стоит проверить Ваш АПИ-ключ 
            </p>    
        </div>       
    </div> 
    <?php    
       
        }else{
    ?>
    <?php 
    if(!$storelandPaymentTypes){?>
        <div class = 'alert-danger'>
            <h1>Не удалось получить типы оплат из Storeland </h1>
            <p class = 'lead'>
                <a href="http://storeland.imb-service.ru/info">Пройдитесь по пунктам документации </a> и проверьте, всё ли правильно Вы делаете
            </p>    
        </div>
    </div> 
    <?php           
        }else{ 
            ?>   
    
     <div class="row">
        <div class="col-lg-12">
            <?php 
            $save = (isset($_GET['save']))? (($_GET['save'] == 'true')? 'true' : ''):'';
                echo ($save == 'true')?
                     '<div class="alert alert-success">
                        <h1>Изменения успешно применены</h1>
                    </div>
            ' : ''  ;
                   echo ($save == 'false')?
                    '<div class="alert alert-danger">
                        <h1>Не удалось применить изменения</h1>
                    </div>
            ' : ''  ;
             
            ?>
            
            </div>
            <div class="sites-form">
                <?= Html::beginForm(['/settings/payments'], 'post') ?>

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Типы доставок в Storeland</th>
                            <th>Типы доставок в retailCRM</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($storelandPaymentTypes as $key => $value) { ?>
                        <tr>
                            <td>
                                <?= $value ?>
                                <?= Html::hiddenInput('storelandPaymentTypes[]', $key ) ?>
                            </td>
                            <td>
                                <?php
                                $options = ['class' => 'form-control'];
                                if($model != ''){                                    
                                    foreach($model as $paymentloc){
                                       
                                        if($paymentloc->attributes['storelandPaymentTypes'] == $key){
                                            $options = ['class' => 'form-control ','options' =>[$paymentloc->attributes['retailPaymentTypes'] =>['selected' => true]]];
                                            break;
                                        }else{
                                            $options = ['class' => 'form-control'];
                                        }
                                    }
                                }
                                ?>
                                
                                <?=  ($model == '')?                                    
                                        Html::dropDownList('retailPaymentTypes['.$key.']',[],$retailPaymentTypes,$options):
                                        Html::dropDownList('retailPaymentTypes['.$key.']',[],$retailPaymentTypes,$options);                                     
                                ?> 
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                </div>
                <div class="form-group">
                    <a onclick="confirm_delete()"class ="btn btn-danger">
                        Очистить настройки 
                    </a>
                </div>
                <script>
                    function confirm_delete(){
                        var isAdmin = confirm("Вы дейстительно хотите очистить соответсия?");
                        if(isAdmin == true){
                            window.location.reload(true);
                            window.location.replace('http://storeland.imb-service.ru/settings/payments?delete=true');
                        }
                    }
                </script>
            </div>                
        </div>             
    </div>
</div>

            
        <?php }
    }
    ?>
    
<?php
if(!empty($_SESSION['freeze']) && $_SESSION['freeze']===1):    
?>

<div id="freeze" class="modal fade bd-example-modal-lg" keyboard="false" aria-labelledby="myLargeModalLabel" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="content modal-main alert">
                <h1>
                    Ваш аккаунт заморожен
                </h1>

                <p>
                    Для разморозки необходимо пополнить счёт
                </p>
            </div>
        </div>
    </div>
</div>

<script>
window.onload = function() {
    $('#freeze').modal({
          backdrop: 'static',
          keyboard: false
    });
};

</script>
<?php endif; ?>


<?php if(isset($_SESSION['active']) && $_SESSION['active']===0 && (empty($_SESSION['freeze']) || $_SESSION['freeze']===0) ):  ?>

<div id="active" class="modal fade bd-example-modal-lg" keyboard="false" aria-labelledby="myLargeModalLabel" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="content modal-main alert">
                <h1>
                    Ваш аккаунт выключен
                </h1>

                <p>
                    Синхронизация заказов проводиться не будет
                </p>
            </div>
        </div>
    </div>
</div>

<script>
window.onload = function() {
    $('#active').modal('show');
};

</script>
<?php endif; ?>

