<?php
 
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $retailStatuses array */
/* @var $storelandStatuses array */
/* @var $shops array */
$this->title = Yii::t('app', 'Соответствие статусов заказов ');
?>
<div class="sites-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php 
    if(!$retailStatuses){?>
        <div class = 'alert-danger'>
            <h1>Не удалось получить статусы заказов из retailCRM</h1>
            <p class = 'lead'>
                Возможно,стоит проверить Ваш АПИ-ключ
            </p>    
        </div>  
     </div> 
    <?php    
       
        }else{
    ?>
    <?php 
    if(!$storelandStatuses){?>
        <div class = 'alert-danger'>
            <h1>Не удалось получить статусы заказов из Storeland</h1>
            <p class = 'lead'>
                <a href="http://storeland.imb-service.ru/info">Пройдитесь по пунктам документации </a> и проверьте, всё ли правильно Вы делаете
            </p>  
        </div>      
 </div> 
    <?php           
        }else{ 
            ?>   
    
     <div class="row">
        <div class="col-lg-12">
            <?php 
            $save = (isset($_GET['save']))? (($_GET['save'] == 'true')? 'true' : ''):'';
                echo ($save == 'true')?
                     '<div class="alert alert-success">
                        <h1>Изменения успешно применены</h1>
                    </div>
            ' : ''  ;
                   echo ($save == 'false')?
                    '<div class="alert alert-danger">
                        <h1>Не удалось применить изменения</h1>
                    </div>
            ' : ''  ;
             
            ?>
            
            </div>
            <div class="sites-form">
                <?= Html::beginForm(['/settings/status'], 'post') ?>

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Статусы заказов в Storeland</th>
                            <th>Статусы заказов в retailCRM</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($storelandStatuses as $key => $value) { ?>
                        <tr>
                            <td>
                                <?= $value ?>
                                <?= Html::hiddenInput('storelandStatuses[]', $key ) ?>
                            </td>
                            <td>
                                <?php
                                $options = ['class' => 'form-control'];
                                if($model != ''){                                                                             
                                    foreach($model as $statusloc){

                                        if((int)$statusloc->attributes['storelandStatus'] === $key){
                                            
                                            $options = ['class' => 'form-control','options' =>[$statusloc->attributes['retailStatus'] =>['selected' => true]]];
  //                                         $options = ['class' => 'form-control'];
                                            break;
                                        }else{
                                            $options = ['class' => 'form-control'];
                                           
                                        }
                                    }
                                }
                                ?>
                                
                                <?=  ($model == '')
                                        ? Html::dropDownList('retailStatuses['.$key.']',[],$retailStatuses,$options)
                                        : Html::dropDownList('retailStatuses['.$key.']',[],$retailStatuses,$options);                                     
                                ?> 
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                </div>
                <div class="form-group">
                    <a onclick="confirm_delete()"class ="btn btn-danger">
                        Очистить настройки 
                    </a>
                </div>
                <script>
                    function confirm_delete(){
                        var isAdmin = confirm("Вы дейстительно хотите очистить соответсия?");
                        if(isAdmin == true){
                            window.location.reload(true);
                            window.location.replace('http://storeland.imb-service.ru/settings/status?delete=true');
                        }
                    }
                </script>
            </div>                
        </div>             
    </div>
</div>

            
        <?php }
    }
    ?>
    

<?php
    require_once ROOT.'views/layouts/active.php';