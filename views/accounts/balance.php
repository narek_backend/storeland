<?php
$this->title = 'Недостаточно средств на счету';
?>
<div class = 'container'>
    <div class="row"> 
        <div class="col-sm-10">
            <h1 class=" alert">На вашем счету недостаточно средств!</h1>
            <p class="alert-dismissable lead">Пополните счёт и попробуйте активировать модуль снова. </p>

        </div>   
    </div>  
</div>        
        
  