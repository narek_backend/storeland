<?php
$this->title = 'Активация';
?>
    <div class="row">
        <div class="col-lg-12">
                      <table class="table table-bordered" style="margin-top: 20px">
                <thead>
                    <tr>                          
                        <th>
                            RetailCRM - аккаунт
                        </th>
                    </tr>
                    <tr>                          
                        <th>
                            Активность
                        </th>
                    </tr>
                    <tr>                          
                        <th>
                            Замороженность
                        </th>
                    </tr>     
                    <tr>                          
                        <th>
                           ClientId
                        </th>
                    </tr>                          
                </thead>
                <tbody> 
                    <tr>                          
                        <tb>
                           <?= $model->attributes['retailAPIURL'] ?>
                        </tb>
                    </tr>
                    <tr>                          
                        <tb>
                            <?= $model->attributes['active'] ?>
                        </tb>
                    </tr>
                    <tr>                          
                        <tb>
                             <?= $model->attributes['freeze'] ?>
                        </tb>
                    </tr>     
                    <tr>                          
                        <tb>
                            <?= $model->attributes['uniqueAccountHash'] ?>
                        </tb>
                    </tr>   
                </tbody>  
            </table>    
 

        </div>
    </div>