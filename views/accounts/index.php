<?php


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Настройки';

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

?>

<h3>Для корректной работы лучше переходить из раздела "Маркетплейс" вашего retailCRM-аккаунта</h3>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-12">
           
        </div>
    </div>

    <?php
    $flash = Yii::$app->session->getFlash('enpty_reister_form');
    
    if (!empty($flash)) {
        ?><div class="alert alert-danger"><?= $flash ?></div><?php
    }
    ?>
    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['id' => 'storeland-crm-settings']); ?>

            <?= $form->field($model, 'retailAPIURL') ?>

            <?=$form->field($model, 'retailAPIKey') ?>

            <?=$form->field($model, 'storelandAPIURL') ?>

            <?=$form->field($model, 'storelandAPIToken') ?>
                        
            <?= $form->field($model, 'uniqueAccountHash')->hiddenInput()->label(false, ['style'=>'display:none']) ?>

            <div class="form-group">
                <?= Html::submitButton('Активировать', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>                       
            
            <?php ActiveForm::end(); ?>

        </div>
    </div>
    <div class = 'lead center-block'>
        <?php $form = ActiveForm::begin(['id' => 'connection-checked','action' =>"http://storeland.imb-service.ru/settings/"]); ?>   
            <?php if(!empty($_SESSION['clientId'])){ ?>
            <?= Html::submitButton('Перейти к настройкам соотвествтия', ['class' => 'btn btn-outline-primary btn-primary', 'name' => 'settings-button']) ?>
            <?php } ?>        
        <?php ActiveForm::end(); ?>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <?php if (!empty($model->retailAPIURL)) { ?>
            <p>
                Получить или обновить API ключ для retailCRM можно тут:
                <a href="<?= $model->retailAPIURL ?>/admin/api-keys" target="_blank">
                    Интеграция → Ключи доступа к API
                </a>
            </p>
            <?php } ?>
            <p>
                Получить API токен для Storeland можно тут:
                <a href="" target="_blank">
                    Storeland → Настройки
                </a>
            </p>
        </div>
    </div>

</div>
  <?php
    require_once ROOT.'views/layouts/active.php';