<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Models\Accounts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="accounts-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'retailAPIURL')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'retailAPIKey')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'storelandAPIURL')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'storelandAPIToken')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uniqueAccountHash')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
