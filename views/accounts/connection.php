<?php

/* 
 * Тут должна быть форма с настройками, такими как:
 * RetailCRM API URL
 * RetailCRM API Key
 * 
 * Главпункт API логин
 * Главпункт API Token
 */

/* @var model */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Настройки';
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<?php
if(Yii::$app->session->isset()){
    ?>
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="form-group">
                <?= Html::submitButton('Активировать', ['class' => 'btn btn-primary', 'name' => 'contact-button'])?>
            </div>
    <?php 
}
else
{
    die('На этом этапе возникла ошибка');
}