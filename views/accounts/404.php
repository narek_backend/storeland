<?php
$this->title = 'Настройки';
?>
<div class = 'container'>
    <div class="row"> 
        <div class="col-sm-10">
            <h1 class=" alert">Не удалось получить уникальный идентификатор пользователя!</h1>
            <p class="alert-dismissable lead"> Возможно период сессии истёк, или Вы перешли на сайт по прямой ссылке,а не через Маркетплейс  вашего <a href = "https://www.retailcrm.ru/" >retailCRM</a>-аккаунта. </p>
            <p class = 'lead'>
            Попробуйте зайти через Маркетплейс вашего <a href = "https://www.retailcrm.ru/" >retailCRM</a>-аккаунта.
            </p>
        </div>   
    </div>  
</div>        
        
  