<script>
window.setTimeout(function() {
    window.location = '<?=$model->retailAPIURL?>/admin/integration/storelandcrm/edit#t-main';
  }, 10000);
  
</script>


<div id="freeze" class="modal fade bd-example-modal-lg" keyboard="false" aria-labelledby="myLargeModalLabel" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="content modal-main">
                <h1>
                    Данная операция недоступна
                </h1>

                <p>
                    Вы будете перенаправлены в настройки модуля в  маркетплейсе Вашего retailCRM-аккаунта в течение <elem  id="clockdiv"  class="seconds"> 10 </elem> секунд                                                                  
                </p>

                  </div>
            </div>
        </div>
    </div>
</div>
<script>
function getTimeRemaining(endtime) {
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);

    return {
      'total': t,

      'seconds': seconds
    };
}
 
function initializeClock(id, endtime) {
  var clock = document.getElementById(id);

  var secondsSpan = clock;
 
  function updateClock() {
    var t = getTimeRemaining(endtime);

    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
 
    if (t.total <= 0) {
      clearInterval(timeinterval);
    }
  }
 
  updateClock();
  var timeinterval = setInterval(updateClock, 1000);
}
 
var deadline = new Date(Date.parse(new Date()) + 10*1000); // for endless timer
initializeClock('clockdiv', deadline);
</script>    
<script>
window.onload = function() {
    $('#freeze').modal({
          backdrop: 'static',
          keyboard: false
    });
};

</script>