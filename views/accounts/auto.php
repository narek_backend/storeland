<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Авторизация';?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-12">
           
        </div>
    </div>

    <?php
    $flash = Yii::$app->session->getFlash('enpty_reister_form');
    
    if (!empty($flash)) {
        ?><div class="alert alert-danger"><?= $flash ?></div><?php
    }
    ?>
    <div class="row">
        <div class="col-lg-12">            
            <form method="POST">
                <div class="form-group">
                    <label for="retailCRMURL">Введите Ваш адресс retauilCRM-аккаунта</label>
                    <input type="text" class="form-control" name ="retailCRMURL" id="retailCRMURL"> 
                </div>
                <input type="submit" class="btn btn-primary">
            </form>
            <?php
            if (!empty($errors)):?>
            <p class = "alert alert-danger">
                <?= $errors ?>
            </p>

            <?php endif ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
        </div>
    </div>
<?php
//    echo '<div class="row">
//        <div class="col-lg-12">
//            <p>POST:</p>
//            <pre>' . var_export($_POST, true) . '</pre>
//        </div>
//        <div class="col-lg-12">
//            <p>$_GET:</p>
//            <pre>' . var_export($_GET, true) . '</pre>
//        </div>
//        <div class="col-lg-12">
//            <p>$_SESSION:</p>
//            <pre>' . var_export($_SESSION, true) . '</pre>
//        </div>
//    </div>';
    ?>
</div>
