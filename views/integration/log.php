

<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
?>
<?php
$this->title = Yii::t('app', 'Лог ошибок');  

if($order != null){
?>
<div class="tab-content">    
    <div class = "center-block">
        <table class="table table-bordered" style="margin-top: 20px">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>ID заказа в Storeland</th>
                    <th>ID заказа в retailCRM</th>
                    <th>Ошибка</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th><?=$order->attributes['id'] ?></th>
                    <th><?=$order->attributes['externalId'] ?></th>
                    <th><?=$order->attributes['retailId'] ?></th>
                    <th><?=($order->attributes['log'] == '') ?'Ошибок нет':$order->attributes['log']   ?></th>
                </tr>

            </tbody>                       
        </table>
        
     </div>
    <div class = "large">
        <form method = "POST" >
            <a class ="btn btn-info" href ="http://storeland.imb-service.ru/integration/log" type = "button" name = "getback" value="Вернуться назад"> Вернуться назад</a>

                
        </form>    
    </div>
</div>
<?php }else{?>
    
<?php

}
?>
<?php
if(!empty($_SESSION['freeze']) && $_SESSION['freeze']===1):    
?>

<div id="freeze" class="modal fade bd-example-modal-lg" keyboard="false" aria-labelledby="myLargeModalLabel" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="content modal-main alert">
                <h1>
                    Ваш аккаунт заморожен
                </h1>

                <p>
                    Для разморозки необходимо пополнить счёт
                </p>
            </div>
        </div>
    </div>
</div>

<script>
window.onload = function() {
    $('#freeze').modal({
          backdrop: 'static',
          keyboard: false
    });
};

</script>
<?php endif; ?>


<?php if(isset($_SESSION['active']) && $_SESSION['active']===0 && (empty($_SESSION['freeze']) || $_SESSION['freeze']===0) ):  ?>

<div id="active" class="modal fade bd-example-modal-lg" keyboard="false" aria-labelledby="myLargeModalLabel" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="content modal-main alert">
                <h1>
                    Ваш аккаунт выключен
                </h1>

                <p>
                    Синхронизация заказов проводиться не будет
                </p>
            </div>
        </div>
    </div>
</div>

<script>
window.onload = function() {
    $('#active').modal('show');
};

</script>
<?php endif; ?>

