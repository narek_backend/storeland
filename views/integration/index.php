

<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
?>
<?php
$this->title = Yii::t('app', 'Синхронизация заказов Storeland в retailCRM');  
?>
<div class="container">
  <center><h1>Синхронизация заказов Storeland в retailCRM</h1></center>
  <ul class="nav nav-tabs nav-justified">
    <li class="<?=($model->attributes['warning'] == 0)? 'active' : '' ?>"><a data-toggle="tab" href="#process">Процесс</a></li>
    <li class="<?=($model->attributes['warning'] == 1)? 'active' : '' ?>"><a data-toggle="tab" href="#settings">Настройки</a></li>
     <li><a  href="http://storeland.imb-service.ru/integration/catalog">Каталог</a></li>
  </ul>


    <div class="tab-content">
      <div id="process" class="tab-pane fade <?= ($model->attributes['warning'] == 0)? 'in active' : '' ?>"> 
          <?php if($model->attributes['warning'] != '1'){?>
          <table class="table table-bordered" style="margin-top: 20px">
            <thead>
                <tr>                          
                    <th>ID заказа в Storeland</th>
                    <th>ID заказа в retailCRM</th>
                    <th>Дата обновления</th>                                      
                </tr>
            </thead>
            <tbody>
            <?php 
            if(count($orders)>0){                                
                foreach ($orders as $order)
                {  

                ?>
                
                <tr  <?= ($order->attributes['errors'] == 1)? 'class = "alert-danger hovertip" onClick = logger('.$order->attributes['id'].')' : 'onclick = "retail(\''.$order->attributes['retailId']."','".$apiUrl.'\')"'?>>                          
                        <td class="font-weight-bold">
                            <?= ($order->attributes['externalId'] != '')?$order->attributes['externalId']: "не удалось определить" ?>
                        </td>
                        <td class="font-weight-bold">
                            <?= ($order->attributes['retailId'] != '')? $order->attributes['retailId'] :  "не удалось определить" ?>                            
                        </td>
                        <td class="font-weight-bold">
                            <?= $order->attributes['lastDate'] ?>
                        </td>
                    </tr> 
                    
                <?php 
                }          
            }
            ?>     
            </tbody>                       
          </table>
          <?php
            echo  LinkPager::widget([
                'pagination' => $pages,
                ]);
            

             }else{
             echo '<h1 class = ""> Синхронизация не будет производиться. Проверьте  <a href = "http://storeland.imb-service.ru/settings">настройки соответствий</a> и <a href = "http://storeland.imb-service.ru/integration"> настройки интеграции</a>!';
          }?>
      </div>
        
    <div id="settings" class="tab-pane fade <?= ($model->attributes['warning'] == 1)? 'in active' : '' ?>">
          <div class="col-lg-12">
              <div class="sites-form">
                <!--<a href="/integration/catalog" class="small">Сформировать каталог из CSV</a>-->
                    <?= Html::beginForm(['/integration#settings'], 'post') ?>

                    <?= HTML::label('Состояние синхронизации: ',null,['class' => 'inline checkbox']);?>  
                    <?= Html::dropDownList("enabled",[
                         'TRUE' => 'Синхронизация включена',
                         'FALSE' => 'Синхронизация отключена',
                     ],[
                         TRUE => 'Синхронизация включена',
                         FALSE => 'Синхронизация отключена',
                     ],['class' => 'form-control ','options' =>[$model->attributes['enabled'] =>['selected' => true]]]);
                    ?>                          

                    <?= HTML::label('Магазин в retailCRM',null,['class' => 'inline checkbox']);?>  
                    <?= Html::dropDownList(
                                        'shop'.'',
                                         $model->attributes['shop'],
                                                    $shops,
                                                    [
                                                        'class' => 'form-control',
                                                        'options' =>[$model->attributes['shop'] =>['selected' => true]]
                                                    ]
                                                ) ?>
                
                <?= HTML::label('Префикс для внешнего ID заказа',null,['class' => 'inline checkbox']);?> 
                <?= Html::input(
                                        'prefix',
                                            'prefix',(empty($model->attributes['prefix']))?'':$model->attributes['prefix'],
                                        [
                                            'class' => 'form-control', 
                                            'name' => 'prefix',
                                        ]
                        ) ?>
                <?php /*
                    <?= HTML::label('Связь товаров с каталогом через: ',null,['class' => 'inline checkbox']);?>  
                
                    <?= Html::dropDownList("catalog",[
                         'xmlId' => 'XML ID',
                         'externalId' => 'External ID',
                         //'article' => 'Артикул',
                     ],[
                         'xmlId' => 'XML ID',
                         'externalId' => 'External ID',
                         //'article' => 'Артикул',
                     ],['class' => 'form-control ','options' =>[$model->attributes['catalog'] =>['selected' => true]]]);
                    ?>                              
                */?>
                    <div class="form-group" style=" margin-top:  20px; ">
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                    </div>
                   <?= Html::endForm() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function logger(id){
        
        window.location.reload(true);
        window.location.replace('http://storeland.imb-service.ru/integration/log?id=' + id);
        
    }
</script>
<script>
    function retail(id,url){
        
        var win = window.open(url +'/orders/' + id +'/edit', '_blank');
        win.focus();
       
        
    }
</script>
  <?php
    require_once ROOT.'views/layouts/active.php';