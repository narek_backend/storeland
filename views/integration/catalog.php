<?php

/* @var $this yii\web\View */

$this->title = 'Интеграция Storeland и retailCRM';
?>
<div class="site-index">

    <div class="body-content">
        <div class="container">
            <div class="row">
                <p>
                   Вы можете преобразовать в формат ICML Яндекс.Маркет файл,полученные В Вашем <a href="<?=$url?>/admin/promotion_yandex_market">аккаунте Stroreland</a> 
                </p>
            </div>
            
            <form method="POST">
                <input class="form-control <?= ($warning===true)?"alert alert-danger":''?>" type="url" name="yandex" required="required" placeholder="Яндекс.маркет" value="<?= empty($_POST['yandex'])?(empty($model->yandex_catalog))?'':$model->yandex_catalog:$_POST['yandex']?>">  
                <input class="form-control form-control-submit" type="submit" name="submit" placeholder="Преобразовать">   
            </form>
        </div>
    </div>
</div>    