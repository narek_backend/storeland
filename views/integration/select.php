<style>
.example-2 .btn-tertiary{color:#555;margin:20%;padding:0;line-height:40px;width:300px;margin:auto;display:block;border:2px solid #555}
.example-2 .btn-tertiary:hover,.example-2 .btn-tertiary:focus{color:#888;border-color:#888}
.example-2 .input-file{width:.1px;height:.1px;opacity:0;overflow:hidden;position:absolute;z-index:-1}
.example-2 .input-file + .js-labelFile{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;padding:0 10px;cursor:pointer}
.example-2 .input-file + .js-labelFile .icon:before{content:"\f093"}
.example-2 .input-file + .js-labelFile.has-file .icon:before{content:"\f00c";color:#5AAC7B}
.js-labelFile {margin: 10px}
</style>
<script>

</script>    
<script>
  window.onload = function(){  
    (function() {
   
  'use strict';
 
  $('.input-file').each(function() {
    var $input = $(this),
        $label = $input.next('.js-labelFile'),
        labelVal = $label.html();
     
   $input.on('change', function(element) {
      var fileName = '';
      if (element.target.value) fileName = element.target.value.split('\\').pop();
      fileName ? $label.addClass('has-file').find('.js-fileName').html(fileName) : $label.removeClass('has-file').html(labelVal);
   });
  });
 
})();}
</script>    

<div class="container">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <p>
    <h5>
        Вы можете загрузить csv-файл с товарами,полученный из админки вашего аккаунта в Storeland, для составления файла формата icml, необходимый для каталога retailCRM
    </h5>
    </p>
    <div class="example-2">
        <div class="form-group">
            <form class="csv-input">
                <input type="file" name="file" id="file" class="input-file">
                 <input type="submit" name="submit" id="file1" class="form-control" >
            </form>
            <label for="file" class="btn btn-tertiary js-labelFile">
                <i class="icon fa fa-check"></i>
                <span class="js-fileName">Загрузить файл</span>
            </label>
           
        </div>
    </div>
</div>    
