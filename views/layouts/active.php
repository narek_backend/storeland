<?php
if(!empty($_SESSION['freeze']) && $_SESSION['freeze']===1):    
?>

<div id="freeze" class="modal fade bd-example-modal-lg" keyboard="false" aria-labelledby="myLargeModalLabel" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="content modal-main">
                <h1>
                    Ваш аккаунт заморожен
                </h1>

                <h4>
                    Для разморозки необходимо пополнить счёт в Вашем аккаунте retailCRM и заново перейти в модуль из Маркетплейса retailCRM
                </h4>
            </div>
        </div>
    </div>
</div>

<script>
window.onload = function() {
    $('#freeze').modal({
          backdrop: 'static',
          keyboard: false
    });
};

</script>
<?php endif; ?>


<?php if(isset($_SESSION['active']) && $_SESSION['active']===0 && (empty($_SESSION['freeze']) || $_SESSION['freeze']===0) ):  ?>

<div id="active" class="modal fade bd-example-modal-lg" keyboard="false" aria-labelledby="myLargeModalLabel" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="content modal-main">
                <h1>
                    Ваш аккаунт выключен
                </h1>

                <h4>
                    Синхронизация заказов проводиться не будет
                </h4>
            </div>
        </div>
    </div>
</div>

<script>
window.onload = function() {
    $('#active').modal('show');
};

</script>
<?php endif; ?>

