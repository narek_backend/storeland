<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>"> 
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
  
<div class="wrap">
    
    <?php
    NavBar::begin([
        'brandLabel' =>'<span><img src="/images/logo_white.svg" width="90" height="45" style="display: inline; margin-right: 10px; margin-top: -3px;" class="img-responsive">' . Yii::$app->name . '</span>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    
    $menu = [
        ['label' => 'Начало', 'url' => ['/accounts']],
        ['label' => 'Инструкция', 'url' => ['/']],
    ];
    $hidden = [
         ['label' => 'Соответствия', 'url' => ['/settings'], 'items' => [
             ['label' => 'Соответствия', 'url' => ['/settings']],

         ]],  
        
    ];
    $support = [['label' => 'Поддержка', 'url' => ['/support']],]           ;
    $sessClientID = Yii::$app->session->get('clientId', null);
    if($sessClientID != ''){
        $menu = array_merge($menu,$hidden);
        $menu = array_merge($menu,$support);
    }
    
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menu
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <a href = "https://im-business.com/">IM-BUSINESS</a> | Студия Сергея Ткаченко <?= date('Y') ?></p>

    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
