<?php


$this->title = 'Интеграция Storeland и retailCRM';
$supportMail = 'mail@im-business.com';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <h2><?= $this->title ?></h2>

                <p>Интеграция разработана в студии Сергея Ткаченко (<a href="https://im-business.com" target="_blank">https://im-business.com</a>)</p>
                
                <p>Поддержка модуля:</p>
                
                <ul>
                    <li>Почта <a href="mailto:<?= $supportMail ?>" target="_blank"><?= $supportMail ?></a></li>
                    <li>Телефон <a href="tel:+74994033442">+7 (499) 403-34-42</a></li>
                </ul>
                
            </div>
        </div>

    </div>
</div>
