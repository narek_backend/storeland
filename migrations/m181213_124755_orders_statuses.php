<?php

use yii\db\Migration;

/**
 * Class m181213_124755_orders_statuses
 */
class m181213_124755_orders_statuses extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders_statuses', [
            'id' => $this->primaryKey(),
            'uniqueAccountHash' => $this->string(255),
            'retailStatus' => $this->string(255),
            'storelandStatus' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181213_124755_orders_statuses cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181213_124755_orders_statuses cannot be reverted.\n";

        return false;
    }
    */
}
