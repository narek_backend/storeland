<?php

use yii\db\Migration;

/**
 * Class m181219_144857_order_sync
 */
class m181219_144857_order_sync extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_sync', [
            'id' => $this->primaryKey(),
            'uniqueAccountHash' => $this->string(255),
            'enabled' => $this->boolean(),
            'warning' => $this->boolean(),
            'lastDate' => $this->dateTime(),
            'shop' => $this->string(255)
        ]);    
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181219_144857_order_sync cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181219_144857_order_sync cannot be reverted.\n";

        return false;
    }
    */
}
