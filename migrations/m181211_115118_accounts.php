<?php

use yii\db\Migration;

/**
 * Class m181211_115118_accounts
 */
class m181211_115118_accounts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('accounts', [
            'id' => $this->primaryKey(),
            'retailAPIURL' => $this->string(255),
            'retailAPIKey' => $this->string(255),
            'storelandAPIURL' => $this->string(255),
            'storelandAPIToken' => $this->string(255),
            'uniqueAccountHash' =>$this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181211_115118_accounts cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181211_115118_accounts cannot be reverted.\n";

        return false;
    }
    */
}
