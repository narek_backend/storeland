<?php

use yii\db\Migration;

/**
 * Class m181219_101105_payments
 */
class m181219_101105_payments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('payments', [
            'id' => $this->primaryKey(),
            'uniqueAccountHash' => $this->string(255),
            'retailPaymentTypes' => $this->string(255),
            'storelandPaymentTypes' => $this->string(255),
        ]);    
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181219_101105_payments cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181219_101105_payments cannot be reverted.\n";

        return false;
    }
    */
}
