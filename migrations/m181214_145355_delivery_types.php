<?php

use yii\db\Migration;

/**
 * Class m181214_145355_delivery_types
 */
class m181214_145355_delivery_types extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('delivery_types', [
            'id' => $this->primaryKey(),
            'uniqueAccountHash' => $this->string(255),
            'retailDeliveryTypes' => $this->string(255),
            'storelandDeliveryTypes' => $this->string(255),
        ]);    
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181214_145355_delivery_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181214_145355_delivery_types cannot be reverted.\n";

        return false;
    }
    */
}
