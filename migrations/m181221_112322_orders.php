<?php

use yii\db\Migration;

/**
 * Class m181221_112322_orders
 */
class m181221_112322_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'uniqueAccountHash' => $this->string(255),
            'errors' => $this->boolean(),
            'lastDate' => $this->dateTime(),
            'externalId' => $this->string(255),
            'retailId' => $this->string(255),
            'direction' => $this->string(255),
            ]);   
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181221_112322_orders cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181221_112322_orders cannot be reverted.\n";

        return false;
    }
    */
}
