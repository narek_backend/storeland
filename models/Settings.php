<?php
namespace app\models;

use app\models\Accounts;
use app\models\OrdersStatuses;
use app\models\DeliveryTypes;
use app\models\Payments;
use app\models\OrderSync;

class Settings{
    public static function checkSettings(){
        $warning= false;
        
        if(!isset($_SESSION['clientId']) || $_SESSION['clientId'] ==''){
            return TRUE ;                
        }else{
            $clientId = $_SESSION['clientId'];
        }
        
        
        $status =  new \app\models\OrdersStatuses;
        $status = ($status::findAll(["uniqueAccountHash" => $clientId]) == null)?null:$status::findAll(["uniqueAccountHash" => $clientId]);
        $delivery = new \app\models\DeliveryTypes;
        $delivery = ($delivery::findAll(["uniqueAccountHash" => $clientId]) == null)?null:$delivery::findAll(["uniqueAccountHash" => $clientId]);
        $payments =  new \app\models\Payments();
        $payments = ($payments ::findAll(["uniqueAccountHash" => $clientId]) == null)?null:$payments ::findAll(["uniqueAccountHash" => $clientId]);
        
        if($payments==null || $delivery==null || $status==null ){
            $warning = TRUE;            
        }
        return $warning;
    } 
}