<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders_statuses".
 *
 * @property int $id
 * @property string $uniqueAccountHash
 * @property string $retailStatus
 * @property string $storelandStatus
 */
class OrdersStatuses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders_statuses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uniqueAccountHash', 'retailStatus', 'storelandStatus'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uniqueAccountHash' => 'Unique Account Hash',
            'retailStatus' => 'Retail Status',
            'storelandStatus' => 'Storeland Status',
        ];
    }
}
