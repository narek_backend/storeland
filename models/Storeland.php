<?php

namespace app\models;

use Yii;
use yii\base\Model;


class Storeland 
{
    private $url;
    private $secret_key;
    
    public function __construct($url,$secret_key){
        $this->url = $url;
        $this->secret_key = $secret_key;
        return true;
    }
    public function checkYmlUrl($inputXmlFile){
      
        if(strpos($inputXmlFile,'export/')==false){
            //return false;
        }
        try{
            if( ! ( $ch = curl_init() ) )
               return false;

            $options = [
                CURLOPT_URL            => $inputXmlFile,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_FAILONERROR => 1,
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTPHEADER     => [
                    'User-Agent' => 'Mozilla/5.0 (Windows NT 5.1; rv:34.0) Gecko/20100101 Firefox/34.0',
                ]
            ];

            curl_setopt_array($ch, $options);
            $file = curl_exec( $ch );
            $xml =$file;
            if(!is_string($xml)){
                return false;
            }
            $xml = str_replace('Ñ','', $file);


        } catch(Exception $e){
           return false;
        }
        
        return $xml;
    }

    public function getPaymentTypes() {
        $url =$this->url;

        if (false === stripos($url, 'http://')) {
             if (false === stripos($url, 'https://')) {
                 return false;
             }
        }
        if (false === stripos($url, '.')) {
                 return false;
        }
        $secret_key = $this->secret_key;
        try{
            $ch = curl_init($url.'/api/v1/orders/get_list');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
                'secret_key' => $secret_key,
                'per_page' => 1000,
            )));

        } catch (Exception $e){
            return false;
        }

        try{
            $resultSrc = curl_exec ($ch);
            if(!$resultSrc) return false;
        } catch (Exception $e){
            return false;
        }

        $result = json_decode($resultSrc , true);
        curl_close ($ch);        
        $statuses  = [];
        foreach ($result['data'] as $order)
        {            
            $statuses[$order['payment_name']['value']] = $order['payment_name']['value'];            
        }
        
        if(count($statuses)>0){
            return $statuses;
        }else{
            return false;
        }                              
    }

    public function getDeliveryTypes() {
        $url =$this->url;

        if (false === stripos($url, 'http://')) {
             if (false === stripos($url, 'https://')) {
                 return false;
             }
        }
        if (false === stripos($url, '.')) {
                 return false;
        }
        $secret_key = $this->secret_key;
        try{
            $ch = curl_init($url.'/api/v1/orders/get_list');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
                'secret_key' => $secret_key,
                'per_page' => 1000,
            )));

        } catch (Exception $e){
            return false;
        }

        try{
            $resultSrc = curl_exec ($ch);
            if(!$resultSrc) return false;
        } catch (Exception $e){
            return false;
        }

        $result = json_decode($resultSrc , true);
        curl_close ($ch);
        
        $statuses  = [];
        foreach ($result['data'] as $order)
        {            

            foreach ($order['line']['value'] as $line){
                foreach ($line as $key=>$smthng){
                    if($key=='order_line_art_number' && $smthng['value'] == 'Доставка'){
                        $statuses[$line['order_line_name']['value']] = $line['order_line_name']['value'];
                        break;
                    }                          
                }
            }
            
        }

        if(count($statuses)>0){
            return $statuses;
        }else{
            return false;
        }                              
    }
    
    public function getOrdersStatuses() {
        $url =$this->url;

        if (false === stripos($url, 'http://')) {
             if (false === stripos($url, 'https://')) {
                 return false;
             }
        }

        if (false === stripos($url, '.')) {
                 return false;
        }
        $secret_key = $this->secret_key;
        try{
            $ch = curl_init($url.'/api/v1/order_statuses/get_list');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
            'secret_key' => $secret_key,
            'per_page' => 10,
            )));

        } catch (Exception $e){
            return false;
        }

        try{
            $resultSrc = curl_exec ($ch);
            if(!$resultSrc) return false;
        } catch (Exception $e){
            return false;
        }

        $result = json_decode($resultSrc , true);
        if(empty($result)){
            return false;
        }
        curl_close ($ch);

        $statuses  = [];
        foreach ($result['data'] as $status){
            $statuses[$status['order_status_id']['value']] = $status['order_status_name']['value'];
        }
        if(count($statuses)>0){
            return $statuses;
        }else{
            return false;
        }                              
    }
    
    public function getOrders($count = 10,$time=''){
        $page = 1;
        $result = null;
        set_time_limit(20);
        $i = 1;
        $last_date = '';
        do{
            $i ++;
            $url =$this->url;

            if (false === stripos($url, 'http://')) {
                 if (false === stripos($url, 'https://')) {
                     return false;
                 }
            }
            if (false === stripos($url, '.')) {
                     return false;
            }
            
            $secret_key = $this->secret_key;
            try{
                $ch = curl_init($url.'/api/v1/orders/get_list');
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 5);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
                'secret_key' => $secret_key,
                'page'=>$page,
                'per_page' => 1,
                )));

            } catch (Exception $e){
                return false;
            }

            try{
                $resultSrc = curl_exec ($ch);
                if(!$resultSrc) return false;
            } catch (Exception $e){
                return false;
            }
            $resultSrc =json_decode($resultSrc,true);
            if($resultSrc['status'] == "ok" || $resultSrc['status'] == "OK" ){
                foreach ($resultSrc['data'] as $order){
                    $order_time = new \DateTime;
                    $order_time = $order_time->setTimestamp($order['created_at']['value']);
                    $order_time = $order_time->format("Y-m-d H:i:s");
                    if (stristr($this->url, 'barexshop.ru') === FALSE){
                        if($order_time>$time){
                            $result[] = $order;
                        }
                    }else{
                        $result[] = $order;
                    }
                }
                \Yii::debug('URL: '.$this->url);
                \Yii::debug($result);
            }else{
                $result  = FALSE;
            }
            curl_close ($ch);
            $page++;
            
            if(empty($order_time) || $last_date==$order_time){
                break;
            }else{
               $last_date =  $order_time;
            }
            
            
        }while(strtotime($order_time)>strtotime($time) && !empty($order_time));
        if(!empty($result)){
            return array_reverse($result);
        }else{
            return [];
        }
        
    }
    
    public function getStores(){
        try{
            $ch = curl_init($this->url.'/api/v1/orders/get_list');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
            'secret_key' => $this->secret_key,
            'per_page' => 10,
            )));

        } catch (Exception $e){
            $errors[] = 'Проблемы подключения к Storeland';
        }

        try{
            $resultSrc = curl_exec ($ch);
            if(!$resultSrc) $errors[] = 'Проблема с ответом от Storeland';
        } catch (Exception $e){
            $errors[] = 'Проблема с ответом от Storeland';
        }

        $result = json_decode($resultSrc , true);
        curl_close ($ch);
        if($result['status'] == 'ok'){
            return true;
        }else{
            return false;
        }    
    }
    
    public function csvToXml($file){
        while($content= fgetc($file)){
            var_dump($content);
        }
    }
}
