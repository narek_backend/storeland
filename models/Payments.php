<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property string $uniqueAccountHash
 * @property string $retailPaymentTypes
 * @property string $storelandPaymentTypes
 */
class Payments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uniqueAccountHash', 'retailPaymentTypes', 'storelandPaymentTypes'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uniqueAccountHash' => 'Unique Account Hash',
            'retailPaymentTypes' => 'Retail Payment Types',
            'storelandPaymentTypes' => 'Storeland Payment Types',
        ];
    }
}
