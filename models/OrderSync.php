<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_sync".
 *
 * @property int $id
 * @property string $uniqueAccountHash
 * @property int $enabled
 * @property string $storelandDeliveryTypes
 */
class OrderSync extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_sync';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['enabled','warning'], 'integer'],
           
            [['uniqueAccountHash','shop','catalog','prefix'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uniqueAccountHash' => 'Unique Account Hash',
            'enabled' => 'Enabled',
            'warning' => 'Warning',
            'shop' => 'Shop',
            'lastDate' => 'Last Date',
            'catalog' => 'Catalog',
            'prefix' => "Prefix",
            
        ];
    }
}
