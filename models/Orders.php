<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property string $uniqueAccountHash
 * @property int $errors
 * @property string $lastDate
 * @property string $externalId
 * @property string $retailId
 * @property string $direction
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['errors'], 'integer'],
            [['lastDate'], 'datetime'],
            [['uniqueAccountHash','log','externalId', 'retailId', 'direction'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uniqueAccountHash' => 'Unique Account Hash',
            'errors' => 'Errors',
            'log' => 'Log',
            'lastDate' => 'Last Date',
            'externalId' => 'External ID',
            'retailId' => 'Retail ID',
            'direction' => 'Direction',
        ];
    }
}
