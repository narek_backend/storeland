<?php

namespace app\models;


class ICML
{
    protected $shop;
    protected $file;
    protected $properties;
    protected $params;
    protected $dd;
    protected $eCategories;
    protected $arrCategories;
    protected $eOffers;
    protected $arrOffers;
    protected $outputFileName;

    private $options;
    private $optionValues;

    public function __construct($inputXmlFile)
    {


        $var = $this->XMLtoArray($inputXmlFile);  
        
        $shop = $var['yml_catalog'][1]['shop'];        
        $categories = $shop['categories'];
        $offers = $shop['offers'];

        foreach ($categories['category'] as $category) {
           
            $this->arrCategories[] = array(
                'id' => $category['@attributes']['id'],
                'parentId' => isset($category['@attributes']['parentId']) ? $category['@attributes']['parentId'] : null,
                'title' => $category['_value']
            );            
        }

        foreach ($offers['offer'] as $idxOffer => $offerList) {

            $isParams = false;

            if ( count( $shop['offers']['offer'][$idxOffer]['param']) > 0) {
                if(!empty($offerList['param'][0])){
                    foreach ($offerList['param'] as $param){
                        $isParams = true;
                        $arrayParams[] = [
                            'name' => $param['@attributes']['name'],
                            'value' => $param['_value'],
                            'code' => 'param-'.$idx
                        ];
                        $idx++;
                    }
                }elseif(!empty ($offerList['param']['@attributes'])){
                    $isParams = true;
                    $arrayParams[] = [
                            'name' => $offerList['param']['@attributes']['name'],
                            'value' => $offerList['param']['_value'],
                            'code' => 'param-'.$idx
                    ];
                }
            }
			
			$newOffer = array(
                'id' => $offerList['@attributes']['id'],
                'url' => $offerList['url'],
                'price' => round($offerList['price']),
                'categoryId' => $offerList['categoryId'],
                'picture' =>  isset($offerList['picture'])? $offerList['picture'] : null,
                'name' => $offerList['name'],
                'productName' => $offerList['name'],
                'vendorCode' => $offerList['vendorCode'],
                'params' => ($isParams) ? $arrayParams : null
            );
			if(isset($offerList['purchasePrice'])){
				$newOffer['purchasePrice'] = round($offerList['purchasePrice']);
			}
            $this->arrOffers[] = $newOffer;
            $idx++;
            unset($arrayParams);
        }
    }

   
    function escape($str, $checkLength = false) {
		if($checkLength && mb_strlen($str) > 255){
			$str = mb_substr($str, 0, 255);
		}
	
		return str_replace(array(
			'"',
			'&',
			'>',
			'<',
			"'",
		), array(
			'&quot;',
			'&amp;',
			'&gt;',
			'&lt;',
			'&apos;',
		), $str);
	}

    private function addCategories() {

        foreach ($this->arrCategories as $category) {

            $e = $this->eCategories->appendChild(
                $this->dd->createElement(
                    'category', htmlspecialchars($category['title'])
                )
            );

            $e->setAttribute('id', $category['id']);

            if ( isset($category['parentId']) && $category['parentId'] > 0) {
                $e->setAttribute('parentId', $category['parentId']);
            }
        }
    }
 public function generateICML()
    {
        $content = '<yml_catalog date="'.date('Y-m-d H:i:s').'">
			<shop>				
				<currencies>
					<currency id="RUR" rate="1"/>
				</currencies>';
        
        $content .= 	 "\r\n".'<categories>'."\r\n";
		foreach($this->arrCategories as $category){
			$content .= "\t".'<category id="'.$category['id'].'"';
            if(!empty($category['parentId'])){
                $content .=  ' parentId="'.$category['parentId'].'" ';
            }
            $content .= '> ';
            $content.=$this->escape($category['title']).'</category>'."\r\n";
		}
		$content .= '</categories>'."\r\n";
        
        $content .= '<offers>'."\r\n";
		foreach($this->arrOffers as $offer){

			$content .= '<offer id="'.$offer['id'].'" productId="'.$offer['id'].'" >'."\r\n";
            $content .= "\t".'<article>'.$this->escape($offer['vendorCode']).'</article>'."\r\n";
            $content .= "\t".'<categoryId>'.$this->escape($offer['categoryId']).'</categoryId>'."\r\n";
            $content .= "\t".'<name>'.$this->escape($offer['productName']).'</name>'."\r\n";
            $content .= "\t".'<productName>'.$this->escape($offer['productName']).'</productName>'."\r\n";
            //$content .= "\t".'<vendorCode>'.$this->escape($offer['vendorCode']).'</vendorCode>'."\r\n";
            $content .= "\t".'<price>'.$this->escape($offer['price']).'</price>'."\r\n";
			if(isset($offer['purchasePrice']) && $offer['purchasePrice'] > 0){
				$content .= "\t".'<purchasePrice>'.$this->escape($offer['purchasePrice']).'</purchasePrice>'."\r\n";
			}
            $content .= "\t".'<picture>'.$this->escape($offer['picture']).'</picture>'."\r\n";
            $content .= "\t".'<url>'.$this->escape($offer['url']).'</url>'."\r\n";
            
            if ( !empty( $offer['params'])) {
                foreach ( $offer['params'] as $k=>$v) {
                    $content .= "\t".'<param name="'.$v['name'].'" code="'.$v['code'].'">'.$this->escape($v['value']).'</param>'."\r\n";
                }
            }
			$content .= '</offer>'."\r\n";
		}
		$content .= '</offers>'."\r\n";

           $content .='</shop></yml_catalog>';
		
		return $content;

        
    }	
    private function addOffers()
    {

        foreach ($this->arrOffers as $offer) {

            $e = $this->eOffers->appendChild($this->dd->createElement('offer'));

            $e->setAttribute('id', $offer['id']);
            $e->setAttribute('productId', $offer['id']);

            $e->appendChild($this->dd->createElement('categoryId'))
                ->appendChild(
                    $this->dd->createTextNode( $offer['categoryId'])
                );


            /**
             * Name & price
             */
            $e->appendChild($this->dd->createElement('productName'))
                ->appendChild($this->dd->createTextNode($offer['productName']));

            $e->appendChild($this->dd->createElement('name'))
                ->appendChild($this->dd->createTextNode($offer['productName']));

            $e->appendChild($this->dd->createElement('vendorCode'))
                ->appendChild($this->dd->createTextNode($offer['vendorCode']));

            $e->appendChild($this->dd->createElement('price'))
                ->appendChild($this->dd->createTextNode($offer['price']));


            $e->appendChild($this->dd->createElement('xmlId'))
                ->appendChild($this->dd->createTextNode($offer['id']));

            $e->appendChild($this->dd->createElement('picture'))
                ->appendChild($this->dd->createTextNode($offer['picture']));

            $e->appendChild($this->dd->createElement('url'))
                ->appendChild($this->dd->createTextNode($offer['url']));

            if ( !empty( $offer['params'])) {

                foreach ( $offer['params'] as $k=>$v) {

                    $param = $this->dd->createElement('param');
                    $param->setAttribute('name', $v['name']);
                    $param->setAttribute('code', $v['code']);
                    $param->appendChild($this->dd->createTextNode($v['value']));
                    $e->appendChild($param);
                }

                unset($param);

            }

        }
    }

    function XMLtoArray($xml) {
        $previous_value = libxml_use_internal_errors(true);
        $dom = new \DOMDocument('1.0', 'UTF-8');
        //$dom->preserveWhiteSpace = false;
        $dom->loadXml($xml);

        libxml_use_internal_errors($previous_value);

        
        if (libxml_get_errors()) {
            return [];
        }
        return self::DOMtoArray($dom);
    }

    static function DOMtoArray($root) {
        $result = array();

        if ($root->hasAttributes()) {
            $attrs = $root->attributes;
            foreach ($attrs as $attr) {
                $result['@attributes'][$attr->name] = $attr->value;
            }
        }

        if ($root->hasChildNodes()) {
            $children = $root->childNodes;
            if ($children->length == 1) {
                $child = $children->item(0);
                if (in_array($child->nodeType,[XML_TEXT_NODE,XML_CDATA_SECTION_NODE])) {
                    $result['_value'] = $child->nodeValue;
                    return count($result) == 1
                        ? $result['_value']
                        : $result;
                }

            }
            $groups = array();
            foreach ($children as $child) {
                if (!isset($result[$child->nodeName])) {
                    $result[$child->nodeName] = self::DOMtoArray($child);
                } else {
                    if (!isset($groups[$child->nodeName])) {
                        $result[$child->nodeName] = array($result[$child->nodeName]);
                        $groups[$child->nodeName] = 1;
                    }
                    $result[$child->nodeName][] = self::DOMtoArray($child);
                }
            }
        }
        return $result;
    }
}


