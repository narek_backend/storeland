<?php

namespace app\models;

use RetailCrm\ApiClient;
use Yii;
use yii\base\Model;
use app\models\Accounts;
use app\models\OrdersStatuses;
use app\models\DeliveryTypes;
use app\models\Payments;
use app\models\PaymentStatuses;
/**
 * Description of RetailCRM
 *
 * @author admhome
 */
class RetailCRM
{

    /**
     * API URL
     * @var string
     */
    protected $apiUrl;

    /**
     * API Key
     * @var string
     */
    protected $apiKey;

    /**
     * Site
     * @var string
     */
    protected $site;

    /**
     * RetailCRM object
     * @var \RetailCrm\ApiClient
     */
    public $retailCRM;

    /**
     * Метод отправки сформированного результата в API RetailCRM
     * @param bool $success - Успех или нет
     * @param array $result - Массив с результатом
     * @param string $errorMsg - Сообщение об ошибке
     * @return yii response object
     */
    public static function apiResponse($success, $result = [], $errorMsg = '') {
        $data = [
            'code' => 200,
            'success' => $success
        ];

        if (!empty($result)) {
            $data['result'] = $result;
        }

        if (!empty($errorMsg)) {
            $data['errorMsg'] = $errorMsg;
        }

        return Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => \yii\web\Response::FORMAT_JSON,
            'data' => $data,
        ]);
    }

    /**
     * Constructor
     * @param string $apiUrl
     * @param string $apiKey
     * @param string $site
     * @return boolean
     */
    public function __construct($apiUrl, $apiKey, $site = null) {
        $this->apiUrl = $apiUrl;
        $this->apiKey = $apiKey;
        
        $this->site = $site;
        
        $this->retailCRM = new \RetailCrm\ApiClient(
            $this->apiUrl,
            $this->apiKey,
            ApiClient::V5,
            $site
        );

        // получить код типа доставки
        // public function deliveryTypesList()
        $deliveryTypes = $this->retailCRM->request->deliveryTypesList();
        

        return true;
    }

    public function __get($name)
    {
        try {
            return $this->{$name};
        } catch (\Exception $e) {
            echo $e->getTraceAsString();
        }
    }

    /**
     * Get Available Methods of API
     * @return array
     */
    public function getAvailableMethods()
    {
        return get_class_methods($this->retailCRM->request);
    }

    /**
     * Получение списка доступных методов и магазинов для данного ключа
     * @return array


    /**
     * Get client credentials
     * @param string $clientId
     * @return array
     */
    public static function getClientData($clientId = '')
    {
        // получить информацию о пользователе
        echo '123';
        $clientId = (!empty($clientId))
                ? $clientId
                : Yii::$app->request->post('clientId', '');
        
        $account = \app\models\Accounts::findOne([
            'uniqueAccountHash' => $clientId
        ]);

        return [
            'clientId' => $clientId,
            'account' => $account
        ];
    }
    
    public function getShippedOrders($filter = [])
    {
        $localFilter = [
            'deliveryTypes' => [
                $this->deliveryTypeCode
            ],
        ];
        
        $filter = !empty($filter)
                ? array_merge($filter, $localFilter)
                : $localFilter;

        $limit = 100;
        $page = 1;

        $totalPages = 1;

        $orders = [];

        try {
            do {
				// public function ordersList(array $filter = [], $page = null, $limit = null)
                $response = $this->retailCRM->request->ordersList($filter, $page, $limit);

//                echo '<pre>$response: ';
//                var_dump($response);
//                echo '</pre>';
//                die('<pre>[DIE] File "' . basename(__FILE__) . '", line: ' . __LINE__ . '</pre>');

                if ($response->isSuccessful()) {
                    $orders = array_merge($orders, $response->orders);
                    
                    $totalPages = $response->pagination['totalPageCount'];
                    $page++;
                } else {
                    return [];
                }
            } while ($page <= $totalPages);

            return $orders;
        } catch (\Exception $e) {
//            echo '<pre>trace: ' . $e->getTraceAsString() . '</pre>';
//            echo '<pre>$orders count: ' . count($orders) . '</pre>';
//            echo '<pre>$response: ';
//            var_dump($response);
//            echo '</pre>';
//            die('<pre>[DIE] File "' . basename(__FILE__) . '", line: ' . __LINE__ . '</pre>');

            return [];
        }
    }


    /**
     * Получить данные заказа
     * @param int $orderID - ID заказа
     * @return array|string
     */
    public function getOrderData($orderID)
    {
        try {
            $response = $this->retailCRM->request->ordersGet($orderID, 'id');
        } catch (\RetailCrm\Exception\CurlException $e) {
            return "Connection error: " . $e->getMessage();
        }

        if ($response->isSuccessful()) {
            return $response->order;
        } else {
            return sprintf(
                "Error: [HTTP-code %s] %s",
                $response->getStatusCode(),
                $response->getErrorMsg()
            );
        }
    }

    /**
     * Get all shops (sites)
     * @return array
     */
    public function getShops()
    {
        try {
            // public function sitesList()
            $response = $this->retailCRM->request->sitesList();
        } catch (\RetailCrm\Exception\CurlException $e) {
            return "Connection error: " . $e->getMessage();
        }

        if ($response->isSuccessful()) {
        $shops =[];
        foreach($response['sites'] as $site){
            $shops[$site['code']]=$site['name'];
        }            
            return $shops;
        } else {
            return sprintf(
                "Error: [HTTP-code %s] %s",
                $response->getStatusCode(),
                $response->getErrorMsg()
            );
        }
    }
    public function getOrdersStatuses()
    {
        try {
            // public function statusesList()
            $response = $this->retailCRM->request->statusesList();
        } catch (\RetailCrm\Exception\CurlException $e) {
            return "Connection error: " . $e->getMessage();
        }

        if ($response->isSuccessful()) {
            $statuses = [];
            foreach($response->statuses as $status){
                $statuses[$status['code']] = $status['name'];
            }
  
            return $statuses;
            
        } else {
            return sprintf(
                "Error: [HTTP-code %s] %s",
                $response->getStatusCode(),
                $response->getErrorMsg()
            );
        }
    }
    
    public function getDeliveryTypes()
    {
        try {
            // public function statusesList()
            $response = $this->retailCRM->request->deliveryTypesList();
            
        } catch (\RetailCrm\Exception\CurlException $e) {
            return "Connection error: " . $e->getMessage();
        }

        if ($response->isSuccessful()) {
            $statuses = [];
            foreach($response->deliveryTypes as $status){
                $statuses[$status['code']] = $status['name'];
            }
  
            return $statuses;
            
        } else {
            return sprintf(
                "Error: [HTTP-code %s] %s",
                $response->getStatusCode(),
                $response->getErrorMsg()
            );
        }
    }
    public function getPaymentTypes()
    {
        try {
            // public function statusesList()
            $response = $this->retailCRM->request->paymentTypesList();
        } catch (\RetailCrm\Exception\CurlException $e) {
            return "Connection error: " . $e->getMessage();
        }

        if ($response->isSuccessful()) {
            $statuses = [];
            foreach($response->paymentTypes as $status){
                $statuses[$status['code']] = $status['name'];
            }
  
            return $statuses;
            
        } else {
            return sprintf(
                "Error: [HTTP-code %s] %s",
                $response->getStatusCode(),
                $response->getErrorMsg()
            );
        }
    } 
    public function getPaymentStatuses()
    {
        try {
            // public function statusesList()
            $response = $this->retailCRM->request->paymentStatusesList();
        } catch (\RetailCrm\Exception\CurlException $e) {
            return "Connection error: " . $e->getMessage();
        }

        if ($response->isSuccessful()) {
            $statuses = [];
            foreach($response->paymentStatuses as $status){
                $statuses[$status['code']] = $status['name'];
            }
  
            return $statuses;
            
        } else {
            return sprintf(
                "Error: [HTTP-code %s] %s",
                $response->getStatusCode(),
                $response->getErrorMsg()
            );
        }
    }
    public function checkCustomer($phone,$email){
        
        if(!empty($phone)){
            try {
                // public function statusesList()
                $response = $this->retailCRM->request->customersList(['name'=>$phone]);
            } catch (\RetailCrm\Exception\CurlException $e) {
                return "Connection error: " . $e->getMessage();
            }
            
            if(!empty($response['customers'][0])){
                return $response['customers'][0]['id'];
            }else{
                return false;
            }
        }else if(!empty ($email)){
             try {
                // public function statusesList()
                $response = $this->retailCRM->request->customersList(['email'=>$email]);
            } catch (\RetailCrm\Exception\CurlException $e) {
                return "Connection error: " . $e->getMessage();
            }
            
            if(!empty($response['customers'][0])){
                return $response['customers'][0]['id'];
            }else{
                return false;
            }
        }
        return false;        
    }

    public function parse($order,$hash,$prefix='',$catalog='externalId'){                        
        $delivery = new \app\models\DeliveryTypes;
        $statuses = new \app\models\OrdersStatuses;
        $payment =  new \app\models\Payments;
        $payments_statuses =  new \app\models\PaymentStatuses;
        file_put_contents(__DIR__.'/orders.log', json_encode($order,JSON_UNESCAPED_UNICODE),FILE_APPEND);
        
        $order_payment_name = $order['payment_name']['value'];
        $order_payment = (int)$order['order_payment']['value'];
        $order_status = (empty($order['order_status_id']['value']))?'':$order['order_status_id']['value'];
        $i=0;
        $delivery_cost = 0;
        $items = [];
        foreach ($order['line']['value'] as $line){                        
                            
            if($line['order_line_type_id']['value'] === '1'){                    
                $items[$i]['offer'][$catalog]=$line['goods_mod_id']['value'];
                $items[$i]['quantity']=$line['order_line_quantity']['value'];
                $items[$i]['productName'] = $line['order_line_name']['value'];
                $items[$i]['initialPrice'] = $line['order_line_sum']['value'];
                $i++;
            }
            if($line['order_line_type_id']['value'] === '2'){                      
                $order_delivery = $line['order_line_name']['value'];     
                $delivery_cost = $line['order_line_sum']['value'];
            }  
           
            
        }
        $order_discount_percent = (empty($order['order_discount_percent']['value']))? (float)0 : (float)$order['order_discount_percent']['value'];
        $order_discount         = (empty($order['order_discount']['value']))? (float)0 : (float)$order['order_discount']['value'];
        if(empty($order_discount_percent) && empty($order_discount)){
            $amount = $order['order_sum']['value'];
        }else{
            $amount = 0;
            foreach ($items as $item) {
                $amount = $amount + $item['initialPrice'];
            }
            if(!empty($order_discount_percent)) {
                $amount = ($amount - (($amount * $order_discount_percent) / 100)) + $delivery_cost;
            }elseif(!empty($order_discount)){
                $amount = ($amount - $order_discount) + $delivery_cost;
            }
        }


        if(empty($order_delivery) || empty($order_status) || empty($order_payment_name) ){
            return -1;
        }
       
        
        $order_comment = (empty($order['order_comment']['value']))?'':$order['order_comment']['value'];
        $order_person = (empty($order['order_person']['value']))? '': $order['order_person']['value'];
        $order_email = (empty($order['order_email']['value']))?'':$order['order_email']['value'];
        $order_phone = (empty($order['order_phone']['value']))?'':$order['order_phone']['value'];
        
        if(!empty($order_phone) || !empty($order_email)){
            $id = $this->checkCustomer($order_phone,$order_email);            
            
        }
        $created_at = (empty($order['created_at']['value']))?'':date("Y-m-d H:i:s",$order['created_at']['value']);
       
        
        $order_city = (empty($order['order_city']['value']))?'':$order['order_city']['value'];
        $order_address = (empty($order['order_address']['value']))? '' : $order['order_address']['value'];
        $order_region= (empty($order['order_region']['value']))? '' : $order['order_region']['value'];
        $order_country = (empty($order['order_country']['value']))? '' : $order['order_country']['value'];
        $delivery_index =(empty($order['order_zip_code']['value']))?'':$order['order_zip_code']['value'];
         
        $order_externalId = (empty($order['order_num']['value']))? '' : $order['order_num']['value'] ;
        $order_comment = (empty($order['order_comment']['value']))? '' : $order['order_comment']['value'];       
        
        $order_convenient_date = (empty($order['order_convenient_date']['value']))? '' : $order['order_convenient_date']['value'];       
        $order_convenient_hour_from = (empty($order['order_convenient_hour_from']['value']))? '' : $order['order_convenient_hour_from']['value'].':00';       
        $order_convenient_hour_to = (empty($order['order_convenient_hour_to']['value']))? '' : $order['order_convenient_hour_to']['value'].':00';       
        


        
        $order_comment_only_for_staff =  (empty($order['order_comment_only_for_staff']['value']))? '' : $order['order_comment_only_for_staff']['value'];  
        $delivery = $delivery->findOne(['uniqueAccountHash'=>$hash,'storelandDeliveryTypes'=>$order_delivery]);
        $statuses = $statuses->findOne(['uniqueAccountHash'=>$hash,'storelandStatus'=> $order_status]);
        $payment = $payment->findOne(['uniqueAccountHash'=>$hash, 'storelandPaymentTypes'=>$order_payment_name]); 

        $paymentStatuses = $payments_statuses->findOne(['uniqueAccountHash'=>$hash, 'storelandPaymentStatus'=>(string)$order_payment]);                       
        
        $order =[];       
        if(!empty($id)){
                $order['customer']['id'] = $id; 
            }
        $order['externalId'] = $prefix.''.$order_externalId;        
        $order['firstName'] = $order_person;         
        $order['email'] = $order_email;
       // $order['createdAt'] = $created_at;
        $order['phone'] =  $order_phone;
        $order['discountManualAmount'] = $order_discount;
        $order['discountManualPercent'] = $order_discount_percent;
        $order['delivery']['address']['city'] =  $order_city;
        $order['delivery']['address']['city'] =  $order_city;
        $order['delivery']['address']['text'] =  $order_address;
        $order['delivery']['address']['index'] = $delivery_index;
        $order['delivery']['address']['region'] = $order_region;
        $order['delivery']['address']['country'] = $order_country;
        $order['delivery']['cost']  = $delivery_cost;
        
        if(!empty($order_convenient_date)){
            $order['delivery']['date'] = $order_convenient_date;
        }
        if(!empty($order_convenient_hour_from)){
            $order['delivery']['time']['from'] = $order_convenient_hour_from;
        }
        if(!empty($order_convenient_hour_to)){
            $order['delivery']['time']['to'] = $order_convenient_hour_to;
        }
         
        $order['status'] = $statuses['retailStatus'];
        $order['delivery']['code'] = $delivery['retailDeliveryTypes'];
        $order['payments']['0']['amount'] = $amount;
        $order['payments']['0']['type'] = $payment['retailPaymentTypes'];
        $order['payments']['0']['status'] = $paymentStatuses['retailPaymentStatus'];
        $order['items'] =  $items;
        $order['customerComment'] = $order_comment;
        $order['managerComment'] = $order_comment_only_for_staff;
//        var_dump($order);
        return $order;        
    }

    public function create($storeland_order,$hash,$site,$catalog='externalId',$prefix='') { 
        
        try{
            \Yii::debug("Начал парсинг заказа");
            $order = $this->parse($storeland_order,$hash,$prefix,$catalog);
        } catch (yii\base\ErrorException $e){
            \Yii::debug("Ошибка парсинга заказо");
            \Yii::debug($e->getMessage());
            return false;
        }
        \Yii::debug("Парсинг заказа прошел");
        if($order == -1){
            return -1;
        }
        try {
            \Yii::debug("Отправляю заказ");
            \Yii::debug($order);

            $response = $this->retailCRM->request->ordersCreate($order,$site);
            \Yii::debug($response);
        } catch (\RetailCrm\Exception\CurlException $e) {
            \Yii::debug("Ошибка загрузки заказов");
            \Yii::debug($e->getMessage());
            return "Connection error: " . $e->getMessage();
        }
        if ($response->isSuccessful()) {
            $retailId = $response['id'];
            $arrRes = ['retail'=>$retailId,'storeland'=>$storeland_order['order_num']['value']];
            \Yii::debug("Загрузка заказов");
            \Yii::debug($arrRes);
            return $arrRes;              
        }else
        {
            if (isset($response['errors'])) {
                $log = '<ul>';
                foreach ($response['errors'] as $er){
                    $log .= "<li>{$er}</li>";
                }
                $log .= "</ul>";
            }elseif(!empty ($response['errorMsg'])){
                $log = $response['errorMsg'];
            }else{
                $log = '';
            }

            $arrRes = ['retail'=>'не удалось создать','storeland'=>$order['externalId'],'log'=>$log ,'response'=> json_encode($response)];
            \Yii::debug("Загрузка заказов");
            \Yii::debug($arrRes);
            return $arrRes;             
        }            
    }

    public function getCredentials() {
        
        $url = 
            '/api/credentials?apiKey='                                                                                                    
            ;
        $url = $this->apiUrl.$url.$this->apiKey;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $chresponse = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($chresponse, true);

        if(empty($data['credentials'])){
            return false;
        }
        return true;

    }    
    
    public function checkCredentials() {
        
        $url = 
            '/api/credentials?apiKey='                                                                                                    
            ;
        $url = $this->apiUrl.$url.$this->apiKey;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $chresponse = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($chresponse, true);
        
        $neededCredentials = [
            '/api/orders',  
            '/api/orders/create',
            '/api/orders/{externalId}',
            '/api/orders/{externalId}/edit',
            '/api/integration-modules/{code}',
            '/api/integration-modules/{code}/edit',
            '/api/reference/delivery-types',
            '/api/reference/payment-statuses',
            '/api/reference/payment-types',
            '/api/reference/statuses',
            '/api/reference/sites',
            '/api/customers'
        ];
        foreach($neededCredentials as $one)
        { 
            if(array_search($one, $data['credentials']) === false){
                return false;
            }
        }
        return true;

    }
    
    public function activateModule($clientId)
    {
        $parameters = array(
            'integrationCode' => \Yii::$app->params['crmIntegrationCode'],
            'code' => \Yii::$app->params['crmIntegrationCode'],
            'active' => 'false',
            'clientId' => $clientId,
            'baseUrl' => \Yii::$app->params['integrationModule']['baseUrl'],
            'accountUrl' => \Yii::$app->params['integrationModule']['accountUrl'],
            'integrations' => [
            ],
            'name' => \Yii::$app->params['integrationModule']['name'],
        );
        
        try {
            $response = $this->retailCRM->request->integrationModulesEdit($parameters);
            
            if ($response->isSuccessful()) {
                return [
                    'success' => true,
                ];
            } else {
                return [
                    'success' => false,
                    'errCode' => $response->getStatusCode(),
                    'errMsg' => $response->getErrorMsg(),
                ];
            }
        } catch (\Exception $exc) {
            return [
                'success' => false,
                'errCode' => -1,
                'errMsg' => "Connection error: " . $exc->getMessage(),
            ];
        }
    }
}
