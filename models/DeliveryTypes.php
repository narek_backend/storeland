<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "delivery_types".
 *
 * @property int $id
 * @property string $uniqueAccountHash
 * @property string $retailDeliveryTypes
 * @property string $storelandDeliveryTypes
 */
class DeliveryTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delivery_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uniqueAccountHash', 'retailDeliveryTypes', 'storelandDeliveryTypes'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uniqueAccountHash' => 'Unique Account Hash',
            'retailDeliveryTypes' => 'Retail Delivery Types',
            'storelandDeliveryTypes' => 'Storeland Delivery Types',
        ];
    }
}
