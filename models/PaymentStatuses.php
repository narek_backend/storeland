<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property string $uniqueAccountHash
 * @property string $retailPaymentTypes
 * @property string $storelandPaymentTypes
 */
class PaymentStatuses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_statuses';
    } 

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uniqueAccountHash', 'retailPaymentStatus', 'storelandPaymentStatus'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uniqueAccountHash' => 'Unique Account Hash',
            'retailPaymentStatus' => 'Retail Payment Types',
            'storelandPaymentStatus' => 'Storeland Payment Types',
        ];
    }
}
