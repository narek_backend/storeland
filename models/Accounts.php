<?php

namespace app\models;

use Yii;
use app\models\RetailCRM;
use app\models\Storeland; 

/**
 * This is the model class for table "accounts".
 *
 * @property int $id
 * @property string $retailAPIURL
 * @property string $retailAPIKey
 * @property string $storelandAPIURL
 * @property string $storelandAPIToken
 * @property string $uniqueAccountHash
 */
class Accounts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'accounts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['retailAPIURL', 'retailAPIKey', 'storelandAPIURL', 'storelandAPIToken', 'uniqueAccountHash'], 'string', 'max' => 255],
            [['retailAPIURL', 'retailAPIKey', 'storelandAPIURL', 'storelandAPIToken', 'uniqueAccountHash'],  'trim'],
            [['retailAPIURL', 'storelandAPIURL'],  'url', 'defaultScheme' => 'https'],
            [['freeze', 'active'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'retailAPIURL' => Yii::t('app','Retail Api Url'),
            'retailAPIKey' => Yii::t('app','Retail Api Key'),
            'storelandAPIURL' => Yii::t('app','Storeland Api Url'),
            'storelandAPIToken' => Yii::t('app','Storeland Api Token'),
            'uniqueAccountHash' =>  Yii::t('app','Unique Account Hash'),
            'active' =>  'Активность',
            'freeze' => 'Замороженность'
        ];
    }
    
    public static function checkConnection($model)
    {
        $errors=[];
        
        try{
            $client = new \RetailCrm\ApiClient($model['retailAPIURL'],$model['retailAPIKey']);
            $response = $client->request->ordersList();
        } catch (Exception $e) {
            $errors[] = 'Проблемы подключения к RetailCRM';
        }
       
        
        
        if(!$response) {//код 818
            $errors[] = 'Проблема с ответом от RetailCRM.';
        }else{            
            if($response->isSuccessful()){
                               
                
                //
                // Проверка на возможности ключа
                //
                $url = 
                    '/api/credentials?apiKey='                                                                                                    
                    ;
                    
             

                $url = $model['retailAPIURL'].$url.$model['retailAPIKey'];
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $chresponse = curl_exec($ch);
                curl_close($ch);
                $data = json_decode($chresponse, true);
                
                
                
                if(empty($data['credentials']) || count($data['credentials']) < 111) 
                {                    
                    $errors[] = 'У API ключа не хватает прав доступа.(см <a href="http://storeland.imb-service.ru/info#key">Руководство</a>)';
                }

                //
                //
                //
                
                
                
                
                $url = $model['storelandAPIURL'];
                

                if (false === stripos($url, 'http://')) {
                     if (false === stripos($url, 'https://')) {
                         $errors[] = 'Ссылка на Storeland-аккаунт введена неверно';
                     }
                }
                if (false === stripos($url, '.')) {
                         $errors[] = 'Возможно,в ссылке на Storeland-аккаунт отсутсвует домен верхнего уровня (например, ".ru" или ".com")';
                }
                $secret_key = $model['storelandAPIToken'];
                try{
                    $ch = curl_init($url.'/api/v1/orders/get_list');
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
                    'secret_key' => $secret_key,
                    'per_page' => 10,
                    )));
                   
                } catch (Exception $e){
                    $errors[] = 'Проблемы подключения к Storeland';
                }
                
                try{
                    $resultSrc = curl_exec ($ch);
                    if(!$resultSrc) $errors[] = 'Проблема с ответом от Storeland';
                } catch (Exception $e){
                    $errors[] = 'Проблема с ответом от Storeland';
                }
            
                $result = json_decode($resultSrc , true);
                curl_close ($ch);
                if($result['status'] == 'ok'){
                    if(empty($errors)){
                        return true;
                    }else{
                        return $errors;
                    }
                }else{
                   $errors[] = 'Неправильный Storeland Token или Url';
                }
            } else {
                 $errors[] = 'Проблема с подключением к RetailCRM';
            }
        }
        return $errors;
    }
        
    public static function getModel($hash='')
    {

        $clientID =($hash =='')?$_SESSION['clientId']:$hash;
        $model = \app\models\Accounts::findOne([
                'uniqueAccountHash' => $clientID
            ]);
        return ($model == null) ? null: $model->attributes;
    }
    
    public static function moduleActivate($model)
    {
        $client = new \RetailCrm\ApiClient($model->attributes['retailAPIURL'],$model->attributes['retailAPIKey']);
        
        if($model->attributes['retailAPIURL'] != 'https://u5904sbar-mn1-justhost.retailcrm.ru' && $model->attributes['retailAPIURL'] != 'https://testirovanie.retailcrm.ru'  ){      
            $parameters = array(
                'clientId' => $model->uniqueAccountHash,
                'integrationCode' => \Yii::$app->params['integrationCode'],
                'code' => \Yii::$app->params['code'],            
                'active' => 'true',            
                'baseUrl' => \Yii::$app->params['baseUrl'],
                'accountUrl' => \Yii::$app->params['accountUrl'],
                'systemUrl' =>\Yii::$app->params['systemUrl'],
                'name' => 'Интеграция Storeland c RetailCRM',
                'availableCountries' => [									// Массив ISO кодов стран (ISO 3166-1 alpha-2) для которых доступен модуль (требуется, если модуль не опубликован в маркетплейсе)
                'RU'
                    ],
                'actions' => [												// Относительные пути от базового URL до конкретных методов (массив "Код метода": "Путь", допустимые методы: activity)
                'activity' => '/site/activated.html',],

            );
            $result = $client->request->integrationModulesEdit($parameters);
            if($result->isSuccessful()){
                $this->stdout('result: ' . var_export($result, true));
            }else{
                return false;
            }
        }else{
            return true;
        }    
    }
    
               
    public static function retailUrlValidate($url)
    {
        if(empty($url)){
            return "Введено пустое значение";
        }
        if(strpos($url, 'https://')>-3){            
        }else{
            return "Введите <strong>полный</strong> адресс";
        }
        if(strpos($url, 'retailcrm.ru')>-3){            
        }else{
            return "Введите корректный адресс";
        }
        return true;
    }
           
    public static function generateClientID()
    {
        return md5(time() . mt_rand(0, 20));
    }
    
}
