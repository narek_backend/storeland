<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

use app\models\RetailCRM;
use app\models\Storeland;
use app\models\Accounts;
use app\models\OrdersStatuses;
use app\models\DeliveryTypes;
use app\models\Payments;
use app\models\OrderSync;
use app\models\Settings;
use app\models\Orders;

class OrdersController extends Controller
{
    public function actionSynchronize(){
        $accs_to_sync = new \app\models\OrderSync;
        $accs_to_sync = $accs_to_sync->findAll(['warning' =>['='=>'0'],'enabled'=>['='=>'1'] ]);
        
        foreach ($accs_to_sync as $acc){
            $site = $acc['shop'];
            $hash = $acc->attributes['uniqueAccountHash'];
            $date = $acc->attributes['lastDate'];
            $newdate = new \DateTime();
            $now = new \DateTime();
            $now = $now->format('Y-m-d H:i:s');
            
            $account= \app\models\Accounts::getModel($hash);
            
            $checkConnection =  \app\models\Accounts::checkConnection($account);
            
            if($checkConnection){
                if($account == null){                 
                    continue;
                }
                else
                {
                    $delivery = new \app\models\DeliveryTypes;
                    $statuses = new \app\models\OrdersStatuses;
                    $payment =  new \app\models\Payments();

                    $payment= $payment->findAll(['uniqueAccountHash'=>$hash]);
                    $statuses = $statuses->findAll(['uniqueAccountHash'=>$hash]);
                    $delivery = $delivery->findAll(['uniqueAccountHash'=>$hash]);

                    if($payment == null || $statuses == null || $delivery == null){
                        print 'Настройки не обнаружены'."\r\n";
                        continue;
                    }

                    $apiUrl = $account['retailAPIURL'];
                    $apiKey = $account['retailAPIKey'];                    
                    
                    $url = $account['storelandAPIURL'];
                    $secret_key = $account['storelandAPIToken'];

                    $storeland = new \app\models\Storeland($url, $secret_key);

                    $retail = new \app\models\RetailCRM($apiUrl, $apiKey);

                    $orders = $storeland->getOrders(10,$date);                   
                    
                    if($orders == null){
                        print 'Заказов не найдено'."\r\n";
                        continue;
                    }else{
                        foreach ($orders as $order){      
                            $ids = $retail->create($order,$hash,$site);                              
                            if($ids !== FALSE){                                         
                                $appModelsOrders =  new \app\models\Orders;
                                $appModelsOrders->uniqueAccountHash = $hash;
                                $appModelsOrders->retailId = (string)$ids['retail'];
                                $appModelsOrders->externalId = (string)$ids['storeland'];
                                $appModelsOrders->errors = 0;                                 
                                $appModelsOrders->shop = $acc->attributes['shop']; 
                                $appModelsOrders->direction = '0';                               
                                $appModelsOrders->save();
                                if(!$appModelsOrders->save())
                                    print_r($appModelsOrders->getErrors());
                            }else{
                                print 'Последний этап'."\r\n";
                                continue;
                            }
                        }
                    }               
                }
            }                       
            $acc->lastDate = $now;
            $acc->save();
        }
    }
    
    
}