<?php

namespace app\controllers;

use Yii;
use app\models\Accounts;
use app\models\AccountsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AccountsController implements the CRUD actions for Accounts model.
 */ 
class AccountsController extends Controller
{

    public $enableCsrfValidation = false;
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action)
    {
//        // необходимо для того, чтобы скрипт мог принимать
//        // внешние запросы напрямую
        $this->enableCsrfValidation = false;
//        
        return parent::beforeAction($action);
    }
    public function actionActivate() {
        $model = [];
        if(!empty($_POST['clientId']) && !empty($_POST['systemUrl']) && (!empty($_POST['activity']))){
            $clientId = $_POST['clientId'];
            $systemUrl = $_POST['systemUrl'];
            $activity = $_POST['activity'];
            
            $model = \app\models\Accounts::findOne([
                'uniqueAccountHash' => $clientId
            ]);
            
            if($model == null){
               return $this->render('405',['model'=>$model]);     
            }else{
                if($model->freeze == true){
                    return $this->redirect('/accounts');
                }
                $activity = json_decode($activity,true);

                if(isset($activity['freeze']) && isset($activity['active']) && $activity['freeze'] !=='' &&  $activity['active']!==''){
                    $model->active = (int)$activity['active'];
                    $model->freeze = (int)$activity['freeze'];
                    $model->retailAPIURL= $systemUrl;
                    $model->save();
                    return $this->render('activity',['model'=>$model]);
                }else{
                    return $this->render('405',['model'=>$model]);   
                }                
            } 
        }else{
              return $this->render('405',['model'=>$model]);  
        }
    }


    /**
     * Lists all Accounts models.
     * @return mixed
     */
    public function actionIndex()                                    
    /*{
        $clientID = '';
        $accountData = [];               
        $connection = -1;
        $errors = true;
        $model = [];
        
//         переход из retailCRM
        if (!empty($_POST['clientId'])) {
            $clientID = filter_var(
                \Yii::$app->request->post('clientId', ''),
                FILTER_SANITIZE_STRING); 
            if(!isset($_SESSION)) 
            { 
              \Yii::$app->session->open(); 
            } 
            \Yii::$app->session->set('clientId', $clientID);
            
            $model = \app\models\Accounts::findOne([
                'uniqueAccountHash' => $clientID
            ]);

            if($model != null){                               
                $connection = 1; 
            }else{
                $model= new \app\models\Accounts;
                $model->retailAPIURL = '';
                $model->retailAPIKey ='';
                $model->storelandAPIURL ='';
                $model->storelandAPIToken ='';
                $model->uniqueAccountHash =$clientID;
                $connection = -1; 
            }                   
        }
        if (!empty($_SESSION['clientId'])) {
            $clientID = $_SESSION['clientId'];
            
            $model = \app\models\Accounts::findOne([
                'uniqueAccountHash' => $clientID
            ]);

            if($model != null){                               
                $connection = 1; 
            }else{
                $model= new \app\models\Accounts;
                $model->retailAPIURL = '';
                $model->retailAPIKey ='';
                $model->storelandAPIURL ='';
                $model->storelandAPIToken ='';
                $model->uniqueAccountHash =$clientID;
                $connection = -1; 
            }                   
        }
           
        if(!empty($_GET['account'])){
            if(strpos($_GET['account'], 'retailcrm.') >-3){
                $model_isset = \app\models\Accounts::findOne(['retailAPIURL' => $_GET['account']]);
                if($model_isset == null){
                    $hash = md5($_GET['account'].'abdy'. microtime());

                    $clientID = $hash;
                
               
                    if(!isset($_SESSION)) 
                    { 
                      \Yii::$app->session->open(); 
                    } 
                    \Yii::$app->session->set('clientId', $clientID);

                    $model = \app\models\Accounts::findOne([
                        'uniqueAccountHash' => $clientID 
                    ]);

                    if($model != null){                               
                        $connection = 1; 
                    }else{
                        $model= new \app\models\Accounts;
                        $model->retailAPIURL = $_GET['account'];
                        $model->retailAPIKey ='';
                        $model->storelandAPIURL ='';
                        $model->storelandAPIToken ='';
                        $model->uniqueAccountHash =$clientID;
                        $connection = -1; 
                    }
                }else{
                     $model = $model_isset;
                      \Yii::$app->session->set('clientId', $model->attributes['uniqueAccountHash']);
                }
            }
        }
               

        // форма
        if (!empty($_POST['Accounts'])) {
            
            $accountData = \Yii::$app->request->post('Accounts', []);
            
            if (!empty($accountData['uniqueAccountHash'])) {
                $clientID = filter_var(
                    $accountData['uniqueAccountHash'],
                    FILTER_SANITIZE_STRING
                );
            }

            $model = (\app\models\Accounts::findOne(['uniqueAccountHash' => $clientID]) == null)?  new \app\models\Accounts : \app\models\Accounts::findOne(['uniqueAccountHash' => $clientID]);
            
            $model_isset = \app\models\Accounts::findOne(['uniqueAccountHash' => $clientID]);
            
            if (!empty($accountData['retailAPIURL']) && !empty($accountData['retailAPIKey']) && !empty($accountData['storelandAPIURL']) && !empty($accountData['storelandAPIToken'])) {
                if(!empty($_SESSION['clientId']) ||  !empty($clientID !='')){
                    
                    
                    $model->retailAPIURL = filter_var(
                        $accountData['retailAPIURL'],
                        FILTER_SANITIZE_STRING
                    );
                    $model->retailAPIKey = filter_var(
                        $accountData['retailAPIKey'],
                        FILTER_SANITIZE_STRING
                    );
                    $model->storelandAPIURL = filter_var(
                        $accountData['storelandAPIURL'],
                        FILTER_SANITIZE_STRING
                    );
                    $model->storelandAPIToken = filter_var(
                        $accountData['storelandAPIToken'],
                        FILTER_SANITIZE_STRING
                    );
                    $model->uniqueAccountHash = filter_var(
                        $clientID,
                        FILTER_SANITIZE_STRING
                    );

                    if (!empty($model['retailAPIURL']) && !empty($model['retailAPIKey']) && !empty($model['storelandAPIURL']) && !empty($model['storelandAPIToken'])) {
                        $errors = \app\models\Accounts::checkConnection($model);
                        
                        if($errors=== TRUE){
                            if($model_isset == null){
                                $result = \app\models\Accounts::moduleActivate($model);
                                if($result === false ){
                                    return $this->render('balance');
                                }else{
                                    $model->active=1;
                                    $model->freeze=0;
                                    $model->save();
                                    $connection = 1; 
                                    $this->redirect($model['retailAPIURL'].'/admin/integration/list');                                    
                                }
                            }
                            $model->save();
                            $connection = 1; 
                        } else {  
                            $connection =0;
                        }
                    }
                    else
                    {                                    
                    }   
                }
            }else{
                \Yii::$app->session->setFlash('enpty_reister_form', 'Значения не должны быть пустыми!');
                $model =  new \app\models\Accounts;
                $model->retailAPIURL = $accountData['retailAPIURL'];
                $model->retailAPIKey = $accountData['retailAPIKey'];
                $model->storelandAPIURL = $accountData['storelandAPIURL'];
                $model->storelandAPIToken = $accountData['storelandAPIToken'];
                $model->uniqueAccountHash =$clientID;                        
            }
        }
        
        $isFreezed =  \app\models\Accounts::findOne([
                        'uniqueAccountHash' => $clientID ]);
        if($isFreezed != null && $isFreezed->freeze == true){
            return $this->render('400');
        }
                   
        $errors ='';
        if(empty($clientID  ) && (empty($_SESSION['clientId']))){
            if(empty($_POST['retailCRMURL'])){
                return $this->render('auto',['errors'=>$errors]);             
            }else{
                $errors = \app\models\Accounts::retailUrlValidate($_POST['retailCRMURL']);
                if($errors === TRUE){
                    return $this->redirect('/accounts?account='.$_POST['retailCRMURL']);
                }else{
                      return $this->render('auto',['errors'=>$errors]);             
                }
            }
        }*/   

    {
        $clientID = '';
        $accountData = [];
        $model = NULL;

        /*
         * Активация из retailCRM:
            get: array (
              'account' => 'https://some.retailcrm.ru',
            )
         *
         * Переход из retailCRM:
            post: array (
              'clientId' => '340568390679af548a02bd00fd033cc9',
            )
         * 
         * Переход из формы:
            post: array (
              '_csrf' => 'GU6j4A3DzRzE1JvDxRBe55gRn22_nRhRBQ07KIb_SrJ4eNaOXoySTouHyaS3fg-z21bRBNflexBpRV1FtLcIhw==',
              'Accounts' => 
              array (
                'retailAPIURL' => 'https://u5904sbar-mn1-justhost.retailcrm.ru',
                'retailAPIKey' => '08WjVVOJ8NR1gvraeWA7WyZIu0tHA4Y6',
                'storelandAPIURL' => 'https://im-business.storeland.ru',
                'storelandAPILogin' => 'admin',
                'storelandAPIToken' => 'm3psb1',
                'uniqueAccountHash' => '340568390679af548a02bd00fd033cc9',
              ),
              'contact-button' => '',
            )
         */


        if (!empty(Yii::$app->session->get('clientId', NULL))) {
            $clientID = Yii::$app->session->get('clientId', NULL);
        }
        
        // пришли из retailCRM
        if (!empty(Yii::$app->request->post('clientId', NULL))) {
            $clientID = filter_var(
                    Yii::$app->request->post('clientId', NULL),
                    FILTER_SANITIZE_STRING
                );
        }
//        if($clientID==='d4ee0fbbeb7ffd4fd7asdfdfsdsa922'){
//            echo '<script>alert("косяк")</script>';
//            $clientID = '17f4465c0cbe6a21c49254ad45fee552';
//        }
        // найти запись
        $model = \app\models\Accounts::findOne([
            'uniqueAccountHash' => $clientID
        ]);
        if(!empty($model)){
                $freeze = $model->freeze;
                $acttive = $model->active;                                
                Yii::$app->session->set('freeze', $freeze);
                Yii::$app->session->set('active', $acttive);
        }
        // если пришли из формы
        if (!empty(Yii::$app->request->post('Accounts', []))) {
            $accountData = Yii::$app->request->post('Accounts', []);

            if (!empty($clientID) && $clientID != $accountData['uniqueAccountHash']) {
                // ОПАСНО! Хэши НЕ совпадают.
                // Попросить пользователя заново зарегистрироваться через retailCRM
                Yii::$app->session->set('clientId', NULL);
                Yii::$app->session->setFlash('danger', Yii::t('app', 'Incorrect integration credentials. Please register module from retailCRM site.'));
                return $this->redirect('/');                
            }
            
            if (!empty($accountData['retailAPIURL'])
                && !empty($accountData['retailAPIKey'])
                && !empty($accountData['storelandAPIURL'])
                && !empty($accountData['storelandAPIToken'])
            ) {

//                echo '<pre>$accountData: ' . var_export($accountData, true) . '</pre>';
//                die('<pre>[DIE] ' . basename(__FILE__) . ':' . __LINE__ . '</pre>');

                $retailAPIURL = str_replace('http:/','https:/',filter_var($accountData['retailAPIURL'], FILTER_SANITIZE_STRING));
                $retailAPIKey = filter_var($accountData['retailAPIKey'], FILTER_SANITIZE_STRING);
                $storelandAPIURL = str_replace('http:/','https:/',filter_var($accountData['storelandAPIURL'], FILTER_SANITIZE_STRING));
                $storelandAPIToken = filter_var($accountData['storelandAPIToken'], FILTER_SANITIZE_STRING);
                $uniqueAccountHash = filter_var($accountData['uniqueAccountHash'], FILTER_SANITIZE_STRING);
                
                if (empty($model)) {
                    $model = \app\models\Accounts::find()->where([
                        'retailAPIURL' => $retailAPIURL,
                        'retailAPIKey' => $retailAPIKey,
                        'storelandAPIURL' => $storelandAPIURL,
                        'storelandAPIToken' => $storelandAPIToken,
                    ])->one();

                    if (empty($model)) {
                        $model = new \app\models\Accounts();
                    }
                }

                $model->retailAPIURL = $retailAPIURL;
                $model->retailAPIKey = $retailAPIKey;
                $model->storelandAPIURL = $storelandAPIURL;
                $model->storelandAPIToken = $storelandAPIToken;
               
                $model->uniqueAccountHash = (!empty($clientID))
                        ? $clientID
                        : $uniqueAccountHash;

//                echo '<pre>$model: ' . var_export($model, true) . '</pre>';
//                die('<pre>[DIE] ' . basename(__FILE__) . ':' . __LINE__ . '</pre>');

                $errors = false;

                // Проверка соединения с retailCRM
                $retialCRM = new \app\models\RetailCRM($model->retailAPIURL, $model->retailAPIKey);
                $credentials = $retialCRM->getCredentials();
                
                if (empty($credentials)) {
                    Yii::$app->session->addFlash(
                            'danger',
                            Yii::t('app', 'Can\'t connect to retailCRM')
                        );
                    $errors = true;
                }

                // Проверка методов для ключа
                $checkResult = $retialCRM->checkCredentials();  

                if ($checkResult == false) {
                    Yii::$app->session->setFlash(
                            'danger',
                            Yii::t('app', 'No needed credentials')
                        );
                    $errors = true;
                }
                
                // Проверка соединения с storeland
                $storeland = new \app\models\Storeland($model->storelandAPIURL, $model->storelandAPIToken);
                $stores = $storeland->getStores();
                
                if ($stores === FALSE) {
                    Yii::$app->session->setFlash(
                            'danger',
                            Yii::t('app', 'Can\'t connect to Storeland')
                        );
                    $errors = true;
                }

                if ($errors == false) {
                    // Сохранение модели
                    $model->save();

                    // Повторная активация
                    $activationResult = $retialCRM->activateModule($clientID);

                    if ($activationResult['success'] == true) {
                        Yii::$app->session->setFlash('success', Yii::t('app', 'Module successully activated!'));
                    } else {
                        Yii::$app->session->setFlash(
                                'danger',
                                Yii::t('app', 'Error #{errCode} - {errMessage}', [
                                    'errCode' => $activationResult['errCode'],
                                    'errMessage' => $activationResult['errMsg'],
                                ])
                            );
                    }
                }
            } else {
                Yii::$app->session->setFlash('danger', Yii::t('app', 'Empty integration credentials'));
            }
        }

        if (!empty(Yii::$app->request->get('account', NULL))) {           
                if(!empty($clientID)){
                    $model = \app\models\Accounts::find()->where([
                        'uniqueAccountHash' => $clientID,
                    ])->one();    
                    
                    if(!empty($model)){
                        return $this->render('redirect', [
                            'model' => $model,                          
                        ]);
                    }else{
                        $account = filter_var(
                            Yii::$app->request->get('account', NULL),
                            FILTER_SANITIZE_STRING
                        );
                        $account_to_activate = \app\models\Accounts::findOne([
                            'retailAPIURL' => $account
                        ]);                    
                        if(empty($account_to_activate)){
                            $model = $account_to_activate;
                        }else{                          
                            return $this->redirect('http://storeland.imb-service.ru/accounts');
                        }
                    }
                }else{
                    $account = filter_var(
                        Yii::$app->request->get('account', NULL),
                        FILTER_SANITIZE_STRING
                    );
                    $account_to_activate = \app\models\Accounts::findOne([
                        'retailAPIURL' => $account
                    ]);                    
                    if(empty($account_to_activate)){
                        $model = $account_to_activate;
                    }else{                      
                        return $this->redirect('http://storeland.imb-service.ru/accounts');
                    }
                }            
           
        }
        // Если нет $clientID
        if (empty($clientID)) {
            $clientID = \app\models\Accounts::generateClientID();
        }

        // Если модель пустая - сделать новую с clientID
        if (empty($model)) {
            $model = new \app\models\Accounts();
            $model->uniqueAccountHash = $clientID;
            
            if (!empty($account)) {
                $model->retailAPIURL = $account;
            }
        }

        Yii::$app->session->set('clientId', $clientID);
        $errors = '';
        $connection = '';
        return $this->render('index', [
            'model' => $model,
            'errors' =>$errors,
            'connection' =>$connection ,
            'clientID' => $clientID,
            'accountData' => $accountData,
        ]);
    }



    /**
     * Updates an existing Accounts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Accounts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Accounts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Accounts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Accounts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
