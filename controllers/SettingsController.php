<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\RetailCRM;
use app\models\Storeland;
use app\models\Accounts;
use app\models\OrdersStatuses;
use app\models\DeliveryTypes;
use app\models\Payments;
use app\models\OrderSync;
class SettingsController  extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;       
        return parent::beforeAction($action);
    }


    public function actionIndex(){
        if(!isset($_SESSION['clientId'])){
            return $this->redirect('/accounts');                
        }else{
            $clientId = $_SESSION['clientId'];
        }
        
        $account = new \app\models\Accounts;
        $account = ($account->findAll(['uniqueAccountHash'=>$clientId])==null)?null:$account->findAll(['uniqueAccountHash'=>$clientId]);
        
        if($account == null)
            return $this->redirect('/accounts');
        
        $warning= false;
        
        $status =  new \app\models\OrdersStatuses;
        $status = ($status::findAll(["uniqueAccountHash" => $clientId]) == null)?null:$status::findAll(["uniqueAccountHash" => $clientId]);
        $delivery = new \app\models\DeliveryTypes;
        $delivery = ($delivery::findAll(["uniqueAccountHash" => $clientId]) == null)?null:$delivery::findAll(["uniqueAccountHash" => $clientId]);

        $payments =  new \app\models\Payments();
        $payments = ($payments ::findAll(["uniqueAccountHash" => $clientId]) == null)?null:$payments ::findAll(["uniqueAccountHash" => $clientId]);
        
        if($payments==null )$warning['Оплаты']='Не настроено соответствие типов оплат';
        if($delivery == null)$warning['Доставки']='Не настроено соответствие типов доставок'; 
        if( $status==null)$warning['Статусы']='Не настроено соответствие статусов заказов';
        
        $order_sync = new \app\models\OrderSync;
        $order_sync = ($order_sync->findOne(['uniqueAccountHash' => $clientId]) == null)? null : $order_sync->findOne(['uniqueAccountHash' => $clientId]);
     if($order_sync != null){
            if($warning!=FALSE){       
                $order_sync->warning = TRUE;
                $order_sync->save();
            }else{
                $order_sync->warning = FALSE;
                $order_sync->save();
            }
        }        
        return $this->render('settings',['warning'=>$warning]);
    }


    public function actionStatus()
    {                            
         if(!isset($_SESSION['clientId']) || $_SESSION['clientId'] ==''){
            return $this->redirect('/accounts');                
        }else{
            $clientId = $_SESSION['clientId'];
        }
        
        if(isset($_GET['delete']) and $_GET['delete']  == true){
            $model = new \app\models\OrdersStatuses;
            $model->deleteAll(['uniqueAccountHash'=>$clientId]);
            return $this->redirect('/settings/status');  
            
        }                
        $model = new \app\models\OrdersStatuses;
        $model = ($model::findAll(["uniqueAccountHash" => $clientId]) == null)?null:$model::findAll(["uniqueAccountHash" => $clientId]);

        $retailStatuses = [];
        $storelandStatuses = [];
        $account= \app\models\Accounts::getModel();
        $apiUrl = $account['retailAPIURL'];
        $apiKey = $account['retailAPIKey'];
        $retailCRM = new RetailCRM($apiUrl, $apiKey);
        $retailStatuses =  $retailCRM->getOrdersStatuses();
        $shops = $retailCRM->getShops();
        $storelandURL = $account['storelandAPIURL'];
        $storelandKey = $account['storelandAPIToken'];        
        $storeland = new \app\models\Storeland($storelandURL,$storelandKey);
        $storelandStatuses = $storeland->getOrdersStatuses();
        
        $retailStatusesPOST = \Yii::$app->request->post('retailStatuses', '') ; 
        $storelandStatusesPOST = \Yii::$app->request->post('storelandStatuses', '');
        
        if($clientId!=''&& $retailStatusesPOST != '' && $storelandStatusesPOST != '' && count($storelandStatusesPOST)== count($retailStatusesPOST)){

        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //


            $i = 0;
            
            foreach($retailStatusesPOST as $storeland => $retail ){
                $model = new \app\models\OrdersStatuses;
                $model = ($model::findOne(["uniqueAccountHash" => $clientId,'storelandStatus'=>$storeland]) == null)?'':$model::findOne(["uniqueAccountHash" => $clientId,'storelandStatus'=>$storeland]);
                if($model==''){
                    $model = new \app\models\OrdersStatuses;
                    $model->retailStatus = strval($retail);
                    $model->storelandStatus = strval($storeland);
                    $model->uniqueAccountHash = $clientId;
                    $model->save();
                }else{
                   $model->retailStatus = strval($retail);     
                   $model->save();
                }
                
            }
            return $this->redirect('/settings/status?save=true');
        }
        else{
            $save = false;
            return $this->render('statuses',[
                'retailStatuses' => $retailStatuses,    
                'storelandStatuses' =>$storelandStatuses,            
                'shops' => $shops,
                'save' => $save ,
                'model' =>$model
                ]);
        }

        return $this->render('statuses',[
            'retailStatuses' => $retailStatuses,    
            'storelandStatuses' =>$storelandStatuses,            
            'shops' => $shops,
            'save' => 'noneed',
            'model' =>$model
            ]);        
    }


    public function actionDelivery()
       {                            
        if(!isset($_SESSION['clientId']) || $_SESSION['clientId'] ==''){
            return $this->redirect('/accounts');                
        }else{
            $clientId = $_SESSION['clientId'];
        }
        
        /*if(isset($_GET['delete']) and $_GET['delete']  == true){
            $model = new \app\models\DeliveryTypes();
            $model->deleteAll(['uniqueAccountHash'=>$clientId]);
            return $this->redirect('/settings/delivery');              
        }*/
        $model = new \app\models\DeliveryTypes();
        $model = ($model::findAll(["uniqueAccountHash" => $clientId]) == null) ? null : $model::findAll(["uniqueAccountHash" => $clientId]);
        $retailDeliveryTypes = [];
        $storelandDeliveryTypes = [];
        $account= \app\models\Accounts::getModel();
        $apiUrl = $account['retailAPIURL'];
        $apiKey = $account['retailAPIKey'];
        $retailCRM = new RetailCRM($apiUrl, $apiKey);
        $retailDeliveryTypes =  $retailCRM->getDeliveryTypes();
        $shops = $retailCRM->getShops();
        $storelandURL = $account['storelandAPIURL'];
        $storelandKey = $account['storelandAPIToken'];        
        $storeland = new \app\models\Storeland($storelandURL,$storelandKey);
        $storelandDeliveryTypes = $storeland->getDeliveryTypes();

        /*if($model != null && count($storelandDeliveryTypes) != count($model)) {
              $model = new \app\models\DeliveryTypes();
            $model->deleteAll(['uniqueAccountHash'=>$clientId]);
            return $this->redirect('/settings/delivery');      
        }*/
        $retailDeliveryTypesPOST = \Yii::$app->request->post('retailDeliveryTypes', '') ; 
        $storelandDeliveryTypesPOST = \Yii::$app->request->post('storelandDeliveryTypes', '');
        if($clientId!=''&& $retailDeliveryTypesPOST != '' && $storelandDeliveryTypesPOST != '' && count($retailDeliveryTypesPOST)== count($storelandDeliveryTypesPOST)){
            $i = 0;
            foreach($retailDeliveryTypesPOST as $storeland => $retail ){
                $model = new DeliveryTypes();
                $deliveryType = ($model::findOne(["uniqueAccountHash" => $clientId,'storelandDeliveryTypes'=>$storeland]) == null)? '':$model::findOne(["uniqueAccountHash" => $clientId,'storelandDeliveryTypes'=>$storeland]);

                if(empty($deliveryType)){


                    //var_dump($retail);
                    $model->retailDeliveryTypes = strval($retail);
                    $model->storelandDeliveryTypes = strval($storeland);
                    $model->uniqueAccountHash = $clientId;
                    $model->save();
                }else{
                    $deliveryType->retailDeliveryTypes = strval($retail);
                    $deliveryType->save();
                }

            }
            return $this->redirect('/settings/delivery?save=true');
        }
        else{
            $save = false;
            return $this->render('delivery',[
                'retailDeliveryTypes' => $retailDeliveryTypes ,    
                'storelandDeliveryTypes' =>$storelandDeliveryTypes ,            
                'shops' => $shops,
                'save' => $save ,
                'model' =>$model
                ]);
        }

        return $this->render('delivery',[
            'retailDeliveryTypes' => $retailDeliveryTypes ,    
            'storelandDeliveryTypes' =>$storelandDeliveryTypes ,               
            'shops' => $shops,
            'save' => 'noneed',
            'model' =>$model
            ]);        
    }


    public function actionPaymentStatuses() {
       if(!isset($_SESSION['clientId']) || $_SESSION['clientId'] ==''){
            return $this->redirect('/accounts');                
        }else{
            $clientId = $_SESSION['clientId'];
        }
        
        if(isset($_GET['delete']) and $_GET['delete']  == true){
            $model = new \app\models\PaymentStatuses();
            $model->deleteAll(['uniqueAccountHash'=>$clientId]);
            return $this->redirect('/settings/payment-statuses');              
        }    
        $model = new \app\models\PaymentStatuses();
        
        $model = ($model::findAll(["uniqueAccountHash" => $clientId]) == null)?null:$model::findAll(["uniqueAccountHash" => $clientId]);

        $retailPaymentTypes = [];
        $storelandPaymentTypes = [];
        $account= \app\models\Accounts::getModel();
        $apiUrl = $account['retailAPIURL'];
        $apiKey = $account['retailAPIKey'];
        $retailCRM = new RetailCRM($apiUrl, $apiKey);
        $shops = $retailCRM->getShops();
        $retailPaymentStatuses =  $retailCRM->getPaymentStatuses();
        $storelandPaymentStatuses= [
            '0' => 'Не оплачен',
            '2' => 'Частично оплачен',
            '1' => 'Оплачен',
        ];
                
        
        $retailPaymentStatusesPOST = \Yii::$app->request->post('retailPaymentStatuses', '') ; 
        $storelandPaymentStatusesPOST = \Yii::$app->request->post('storelandPaymentStatuses', '');
       
       
        if($clientId!=''&& $retailPaymentStatusesPOST ){

        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //

            
            $i = 0;
            
            foreach($retailPaymentStatusesPOST as $storeland => $retail ){
                $model = new \app\models\PaymentStatuses();
                $model = ($model::findOne(["uniqueAccountHash" => $clientId,'storelandPaymentStatus'=>$storeland]) == null)? '':$model::findOne(["uniqueAccountHash" => $clientId,'storelandPaymentStatus'=>$storeland]);

                if($model==''){
                    var_dump($retail);
                    $model = new \app\models\PaymentStatuses();
                    $model->retailPaymentStatus = strval($retail);
                    $model->storelandPaymentStatus = strval($storeland);
                    $model->uniqueAccountHash = $clientId;
                    
                    if($model->save(false)) {

                    }
                }else{
                   $model->retailPaymentStatus = strval($retail);     
                    if($model->save(false)) {
                       
                    }
                }

            }
            return $this->redirect('/settings/payment-statuses?save=true');
        }
        else{
            $save = false;
            return $this->render('payment_statuses',[
                'retailPaymentStatuses' => $retailPaymentStatuses ,    
                'storelandPaymentStatuses' =>$storelandPaymentStatuses ,            
                'shops' => $shops,
                'save' => $save ,
                'model' =>$model
                ]);
        }

        return $this->render('payment_statuses',[
            'retailPaymentTypes' => $retailPaymentTypes ,    
            'storelandPaymentTypes' =>$storelandPaymentTypes ,               
            'shops' => $shops,
            'save' => 'noneed',
            'model' =>$model
            ]); 
    }


    public function actionPayments()
       {                            
        if(!isset($_SESSION['clientId']) || $_SESSION['clientId'] ==''){
            return $this->redirect('/accounts');                
        }else{
            $clientId = $_SESSION['clientId'];
        }
        
        /*if(isset($_GET['delete']) and $_GET['delete']  == true){
            $model = new \app\models\Payments();
            $model->deleteAll(['uniqueAccountHash'=>$clientId]);
            return $this->redirect('/settings/payments');  
            
        }                */
        $model = new \app\models\Payments();
        $model = ($model::findAll(["uniqueAccountHash" => $clientId]) == null)?null:$model::findAll(["uniqueAccountHash" => $clientId]);

        $retailPaymentTypes = [];
        $storelandPaymentTypes = [];
        $account= \app\models\Accounts::getModel();
        $apiUrl = $account['retailAPIURL'];
        $apiKey = $account['retailAPIKey'];
        $retailCRM = new RetailCRM($apiUrl, $apiKey);
        $retailPaymentTypes =  $retailCRM->getPaymentTypes();
        $shops = $retailCRM->getShops();
        $storelandURL = $account['storelandAPIURL'];
        $storelandKey = $account['storelandAPIToken'];        
        $storeland = new \app\models\Storeland($storelandURL,$storelandKey);
        $storelandPaymentTypes = $storeland->getPaymentTypes();
        
        /*if($model != null && count($storelandPaymentTypes) != count($model)) {
            $model = new \app\models\Payments();
            $model->deleteAll(['uniqueAccountHash'=>$clientId]);
            return $this->redirect('/settings/payments');      
        }*/
        
        
        $retailPaymentTypesPOST = \Yii::$app->request->post('retailPaymentTypes', '') ; 
        $storelandPaymentTypesPOST = \Yii::$app->request->post('storelandPaymentTypes', '');
        
        if($clientId!=''&& $retailPaymentTypesPOST != '' && $storelandPaymentTypesPOST != '' && count($retailPaymentTypesPOST)== count($storelandPaymentTypesPOST)){

        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //

            
            $i = 0;
            
            foreach($retailPaymentTypesPOST as $storeland => $retail ){
                $model = new \app\models\Payments();
                $model = ($model::findOne(["uniqueAccountHash" => $clientId,'storelandPaymentTypes'=>$storeland]) == null)? '':$model::findOne(["uniqueAccountHash" => $clientId,'storelandPaymentTypes'=>$storeland]);

                if($model==''){
                    var_dump($retail);
                    $model = new \app\models\Payments();
                    $model->retailPaymentTypes = strval($retail);
                    $model->storelandPaymentTypes = strval($storeland);
                    $model->uniqueAccountHash = $clientId;
                    $model->save();
                }else{
                   $model->retailPaymentTypes = strval($retail);     
                   $model->save();
                }

            }
            return $this->redirect('/settings/payments?save=true');
        }
        else{
            $save = false;
            return $this->render('payments',[
                'retailPaymentTypes' => $retailPaymentTypes ,    
                'storelandPaymentTypes' =>$storelandPaymentTypes ,            
                'shops' => $shops,
                'save' => $save ,
                'model' =>$model
                ]);
        }

        return $this->render('payments',[
            'retailPaymentTypes' => $retailPaymentTypes ,    
            'storelandPaymentTypes' =>$storelandPaymentTypes ,               
            'shops' => $shops,
            'save' => 'noneed',
            'model' =>$model
            ]);        
    }
    
}