<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\RetailCRM;
use app\models\Storeland;
use app\models\Accounts;
use app\models\OrdersStatuses;
use app\models\DeliveryTypes; 
use app\models\Payments;
use app\models\OrderSync;
use app\models\Settings;
use app\models\Orders;
use app\models\ICML;
use yii\data\Pagination;

/**
 * AccountsController implements the CRUD actions for Accounts model.
 */
class IntegrationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public $enableCsrfValidation = false;
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    public function actionCheck(){        
        
       
    }

    public function actionLog(){
        if (isset($_POST['getback'])){
             return $this->redirect('/integration');        
        }
        if(!isset($_GET['id'])){
            return $this->redirect('/integration'); 
        }else{
            $orders = new \app\models\Orders;
            $id = $_GET['id'];
            $order = $orders->findOne(['id'=>$id]);
            return $this->render('log',['order'=>$order]);      
        }
    }


    public function actionCatalog($id=''){ 
        if(!empty($id)){
             $model = new \app\models\OrderSync;
            $model = ($model::findOne(["catalog_id" => $id]) == null)?null:$model::findOne(["catalog_id" => $id]);
            if(empty($model->catalog_id)){
                return $this->render('catalog',['url'=>$url,'model'=>$model,'warning'=>true]);   
            }
            
            if(empty($model->yandex_catalog)){
                return $this->render('catalog',['url'=>$url,'model'=>$model,'warning'=>true]);   
            }
            
            $account = \app\models\Accounts::getModel($model->uniqueAccountHash);
            if($account == null){
                 return $this->redirect('/accounts');   
            }

             $url = $account['storelandAPIURL'];
            $secret_key = $account['storelandAPIToken'];

            $storeland = new \app\models\Storeland($url,$secret_key);
            
            $str_to_xml = $storeland->checkYmlUrl($model->yandex_catalog );

            if(empty($str_to_xml)){
                return $this->render('catalog',['url'=>$url,'model'=>$model,'warning'=>true]);  
            }
            $ICML = new \app\models\ICML($str_to_xml);
            $xml = $ICML->generateICML();  
            header('Content-Type: application/xml; charset=utf-8');
            print $xml;
            die();
        }
        
        
        
        if(!isset($_SESSION['clientId']) || $_SESSION['clientId'] ==''){
            return $this->redirect('/accounts');                 
        }else{
            $clientId = $_SESSION['clientId'];
            
        }
        $account = \app\models\Accounts::getModel();
        if($account == null){
             return $this->redirect('/accounts');   
        }
        
        $model = new \app\models\OrderSync;
        $model = ($model::findOne(["uniqueAccountHash" => $clientId]) == null)
            ? new \app\models\OrderSync()
            : $model::findOne(["uniqueAccountHash" => $clientId]);
        
        $apiUrl = $account['retailAPIURL'];
        $apiKey = $account['retailAPIKey'];                
        
        $url = $account['storelandAPIURL'];
        $secret_key = $account['storelandAPIToken'];

        $retail = new \app\models\RetailCRM($apiUrl, $apiKey);
        $shops = $retail->getShops();

        $storeland = new \app\models\Storeland($url,$secret_key);
        if(!empty($_POST)){
            if(empty($_POST['yandex'])){
                return $this->render('catalog',['url'=>$url,'model'=>$model,'warning'=>true]);   
            }else{
               $url = filter_input(INPUT_POST, 'yandex');
               $str_to_xml = $storeland->checkYmlUrl($url);
               if($str_to_xml === false){
                    return $this->render('catalog',['url'=>$url,'model'=>$model,'warning'=>true]);   
               }else{
                   $model->yandex_catalog = $url;
                   if(empty($model->catalog_id)){
                       $model->catalog_id = $model->id * (int)str_replace('.', '', date("d.m.Y"));
                   }
                   $model->uniqueAccountHash = $clientId;
                   $model->shop = reset($shops);
                   $model->save();   
                   return $this->redirect('/integration/catalog/'. $model->catalog_id);   
               }                 
            }
        }                
        
            return $this->render('catalog',['url'=>$url,'model'=>$model,'warning'=>false]);   

    }

    
    /**
     * Lists all Accounts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $warning = \app\models\Settings::checkSettings();
     
        if($warning)
            return $this->redirect('/settings');
        
        $warning = (int)$warning;
        
        if(!isset($_SESSION['clientId']) || $_SESSION['clientId'] ==''){
            return $this->redirect('/accounts');                 
        }else{
            $clientId = $_SESSION['clientId'];
        }

        /*if(isset($_SERVER["HTTP_REFERER"]) && strpos($_SERVER["HTTP_REFERER"] , '://storeland.imb-service.ru/settings') >-3 && strpos($_SERVER["HTTP_REFERER"] , '//storeland.imb-service.ru/settings') != false ){
            
        }else{
            if(isset($_SERVER["HTTP_REFERER"]) && strpos($_SERVER["HTTP_REFERER"] , '://storeland.imb-service.ru/integration') >-3 && strpos($_SERVER["HTTP_REFERER"] , '//storeland.imb-service.ru/integration') != false ){
            
            }else{
                if(isset($_SERVER["HTTP_REFERER"]) && strpos($_SERVER["HTTP_REFERER"] , '://storeland.imb-service.ru/integration/log') >-3 && strpos($_SERVER["HTTP_REFERER"] , '//storeland.imb-service.ru/integration/log') != false ){
                    
                }else
                    {
                    return $this->redirect('/settings'); 
                }                
            }
        }*/
        
        
        
        $account= \app\models\Accounts::getModel();
        if($account == null){
             return $this->redirect('/accounts');   
        }
        
        $apiUrl = $account['retailAPIURL'];
        $apiKey = $account['retailAPIKey'];
        

        
        $url = $account['storelandAPIURL'];
        $secret_key = $account['storelandAPIToken'];
        
        $storeland = new \app\models\Storeland($url, $secret_key);
        $retail = new \app\models\RetailCRM($apiUrl, $apiKey);
        
        $shops = $retail->getShops();
        
        $orders = [];
        $model = new \app\models\OrderSync;
        $model = ($model::findOne(["uniqueAccountHash" => $clientId]) == null)?null:$model::findOne(["uniqueAccountHash" => $clientId]);
        if($model==null){
            $model = new \app\models\OrderSync;
            $lastDate = 'Синхронизация заказов не производилась';            
            $model->enabled=0;
            $date = new \DateTime();
            $date = $date->format('Y-m-d H:i:s');
            $model->lastDate=$date;
            $model->enabled=0;
            $model->warning=1;
            $model->catalog = 'externalId';
            $model->shop = reset($shops);
            $model->uniqueAccountHash = $clientId;               
        }else{
            $model->warning=0;
            $model->save();
            $lastDate = $model->attributes['lastDate'];
        }

        if(isset($_POST['enabled'])){
            if(isset($_POST['shop']) && $_POST['shop']!=''){                
                $model = new \app\models\OrderSync();
                $model = ($model::findOne(["uniqueAccountHash" => $clientId]) == null)? '':$model::findOne(["uniqueAccountHash" => $clientId]);

                 if($model==''){   
                    $model = new \app\models\OrderSync();
                    $model->shop =strval(filter_input(INPUT_POST, 'shop'));  
                    $date = new \DateTime();                    
                    $model->enabled = ($_POST['enabled'] == '0')? 0:1;
                    $model->warning=$warning;
                    $model->prefix = strval((empty(filter_input(INPUT_POST,'prefix')))?'':filter_input(INPUT_POST,'prefix')); 
                    $model->uniqueAccountHash = $clientId;
                    $_POST = [];
                    if(!$model->save()){
                         print_r($model->getErrors());

                    }else{
                        return $this->redirect('/integration');
                    }
                }else{
                   $model->shop = strval(filter_input(INPUT_POST, 'shop'));   
                   $model->prefix = strval((empty(filter_input(INPUT_POST,'prefix')))?'':filter_input(INPUT_POST,'prefix')); 
                   $model->enabled = ($_POST['enabled'] == '0')? 0:1;                   
                   $model->warning = 0;
                   $model->save();                      

                   $_POST = [];
                   if(!$model->save()){
                        print_r($model->getErrors());

                   }else{
                        return $this->redirect('/integration');
                    }
                }                
            }
        }
        if(isset($_POST['catalog']) && $_POST['catalog']!=''){
            $model = new \app\models\OrderSync();
            $model = ($model::findOne(["uniqueAccountHash" => $clientId]) == null)? '':$model::findOne(["uniqueAccountHash" => $clientId]);

            if($model==''){  
                 $model = new \app\models\OrderSync();
                 $model->catalog = filter_input(0, 'catalog');
                 $model->save();
            }else{
                $model->catalog = filter_input(0, 'catalog');
                $model->save();   
            }
        }
        $orders = new \app\models\Orders;
        //$orders = ($orders->findAll(["uniqueAccountHash" => $clientId]) == null)? [] : $orders->findAll(["uniqueAccountHash" => $clientId]);
        $query = Orders::find()->select('*')->where(['uniqueAccountHash'=>$clientId])->orderBy('id DESC');
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize'=>10]);
        $orders = $query->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('index',['apiUrl'=>$apiUrl,'model'=>$model,'lastDate'=>$lastDate,'shops' => $shops,'orders' => $orders,'pages'=>$pages]);        
          
    } 
}
